# GLTF (2.0) support for Radium engine

This document indicates, from the main structures defined in the GLTF2.0 specification, what is supported, 
partly supported or not supported.

For more information about gltf file format and its use by 3D engines/apps visit https://www.khronos.org/gltf/


## Installation

### Requirements
We assume that Radium Engine is in your file system and properly compiled and installed in the directory
`RadiumInstalled`.

### Compilation

To configure the GLTF library and the Radium plugin that uses the library, do as for any Radium based
software. From the repository root directory (where you cloned the GLTF2.0 repository) do : 

1. `mkdir build-release && cd build-release`
2. `cmake -DRadium_DIR=RadiumInstalled/lib/cmake/Radium/ -DQt5_DIR=/path/to/Qt5/ ..`
3. To compile the library and the plugin, type `cmake --build .`
4. To install the library and the plugin, type `cmake --install .`. Installation will allow to use the library 
and the plugin from anywhere on your system but you can use the plugin without installing it.

### Using the plugin in a Radium application
To use the plugin, after installation or from its build tree, just add the appropriate directory into the
plugins search paths of your application.

By default, the plugin is installed in the Radium-Bundle. It will be automatically loaded by applications launched or installed after installing the plugin (on Linux and MacOs).

If it is not the case, do the following :

1. After installation : add the directory `build-release/installed-GNU` on the plugin search path (or the one you 
mention with the option `-DCMAKE_INSTALL_PREFIX=somewhereInYourSystem` you could have given when configuring the 
compilation)
2. from the build tree : add the directory `build-release/src/Plugin/Plugins/lib` on the plugin search path.

### Using the library in your applications
Once install, the library is define in the CMAKE package `build-release/installed-GNU/lib/cmake/RadiumGlTFConfig.cmake`.
This package configure everything you need to use the GLTF library. 

To use the library in an application, you might write the cmake as the following.
```cmake
cmake_minimum_required(VERSION 3.6)
project(myRadiumProject)
# this will find the full Radium dependency
find_package(RadiumGlTF REQUIRED)

add_executable(ApplicationName your_sources)
# This will define include paths and link libraries for your application
target_link_libraries(ApplicationName PUBLIC Radium::Core Radium::Engine RadiumGlTF::RadiumGlTF your_libs)
# This will configure your application as a Radium application (installation mode, plugin usage, ...)
configure_radium_app(
        NAME ApplicationName
)
```
and configure your application using
`cmake -DRadium_DIR=/athToRadiumInstalled/lib/cmake/Radium/ -DQt5_DIR=/path/to/Qt5/ 
-DRadiumGlTF_DIR=/pathToRadiumGlTFInstall/lib/cmake ..`

An example on how to use the library is located into
the `src/Applicaion/` directory.

## GLTF2.0 coverage
The structure of this document is the same than those of the 
[official specification](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0)


* [Concepts](#concepts)
  * [Asset](#asset)
  * [Indices and Names](#indices-and-names)
  * [Coordinate System and Units](#coordinate-system-and-units)
  * [Scenes](#scenes)
    * [Nodes and Hierarchy](#nodes-and-hierarchy)
    * [Transformations](#transformations)
  * [Binary Data Storage](#binary-data-storage)
    * [Buffers and Buffer Views](#buffers-and-buffer-views)
      * [GLB-stored Buffer](#glb-stored-buffer)
    * [Accessors](#accessors)
        * [Accessors Bounds](#accessors-bounds)
        * [Sparse Accessors](#sparse-accessors)
    * [Data Alignment](#data-alignment)   
  * [Geometry](#geometry)
    * [Meshes](#meshes)
      * [Tangent-space definition](#tangent-space-definition)
      * [Morph Targets](#morph-targets)
    * [Skins](#skins)
      * [Skinned Mesh Attributes](#skinned-mesh-attributes)
      * [Joint Hierarchy](#joint-hierarchy)
    * [Instantiation](#instantiation)
  * [Texture Data](#texture-data)
    * [Textures](#textures)
    * [Images](#images)
    * [Samplers](#samplers)
  * [Materials](#materials)
    * [Metallic-Roughness Material](#metallic-roughness-material)
    * [Additional Maps](#additional-maps)
    * [Alpha Coverage](#alpha-coverage)
    * [Double Sided](#double-sided)
    * [Default Material](#default-material)
    * [Point and Line Materials](#point-and-line-materials)
  * [Cameras](#cameras)
    * [Projection Matrices](#projection-matrices)
  * [Animations](#animations)
  * [Specifying Extensions](#specifying-extensions)
* [GLB File Format Specification](#glb-file-format-specification)
  * [File Extension](#file-extension)
  * [MIME Type](#mime-type)
  * [Binary glTF Layout](#binary-gltf-layout)
    * [Header](#header)
    * [Chunks](#chunks)
      * [Structured JSON Content](#structured-json-content)
      * [Binary Buffer](#binary-buffer)

## Concepts
### Asset
The `asset` node ([specification](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#reference-asset))
is parsed when loading a glTF file and its content is logged in the Radium `LogINFO` logger.


### Indices and Names
Names of glTF entities are transmitted to the Radium Engine. These names then serve as keys for several resources management components.

### Coordinate System and Units
glTF uses a right-handed coordinate system that correspond to the coordinate system of Radium.
The test [Boom with axes](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0/BoomBoxWithAxes) pass.

### Scenes
All the scenes are loaded but only the one referenced by the `scene` property is converted to Radium asset.
If `scene`is undefined, the scene `0` is converted.

#### Nodes and Hierarchy
After loading, the node hierarchy defined in the glTF file is flattened. 

> In the next version of Radium (Radium v2), node hierarchy will be maintained. 

#### Transformations
All the glTF transformations are integrated when flattening the nodes tree. 

### Binary Data Storage
#### Buffers and Buffer Views
All buffers are loaded when parsing a glTF file. 
Buffer views will be used to extract data from buffers and fill Radium IO data structure.
The `target` property of a buffer is not used by the loader that infer usage of the buffer from the mesh `accessor` properties.

> the `byteStride` property, defined only for buffer views that contain vertex attributes, is not used in the converter. 
Must take it into account in the near future.

##### GLB-stored Buffer
GLB-stored buffer are transparently managed by the loader.

#### Accessors
Accessors are used to build `RadiumIO` compatible description of the Geometry. 

##### Accessors Bounds
 The `min`and `max`properties are not used as they are recomputed by Radium when building a mesh.

##### Sparse Accessors
 Not tested and not sure they are well managed.
 
##### Data Alignment
For the moment, data are considered as non aligned.

> Must take into account alignement to handle all the test cases.

### Geometry
Only non morphed meshes are managed. 
The loader read all from the file but the Radium converter ignores morphing based animation targets.

#### Meshes
The way a mesh is divided into `primitives` is kept while building radium geometries. 
Each primitive defines a Radium mesh on its own.

Due to restriction in Radium, only the texture coord layer 0 (`TEXCOORD_0`) is converted and used.

> In the next version of Radium, both `TEXCOORD_0` and `TEXCOORD_1` will be made available.

The `COLOR_0` property is not used for now.

> In a near future, this could be incorporated as Radium manage color attributes per vertex.

Weighs and joints are correctly loaded and transmitted to Radium for skeleton based animation.

> Regular nodes and nodes hierarchies attached to a joint are not handled as expected by the gtlf specification.

If normals are not given, they are computed, as well as tangent, according to the GLTF specification.

##### Tangent-space definition
If tangent are not defined, the loader use the [MikkTSpace algorithm](http://image.diku.dk/projects/media/morten.mikkelsen.08.pdf), 
in its original implementation by the author to compute the tangents. This respect the GLTF specification.

##### Morph Targets
Not yet managed by the converter.

#### Skins
Skins are loaded and skeletons are build according to the specification but with the restriction imposed by Radium 
for now (one single mesh per skeleton, no node hierarchy attached to a joint, ...)

### Texture Data
glTF separates texture access into three distinct types of objects: Textures, Images, and Samplers.
For the moment, Radium only represents textures by a single type of object. 
Several conformance tests, where the same image is used with different samplers, do not pass yet.

> In Radium v2, manage textures as an association of images and samplers.

#### Images
Only images defined by a URI to an external file are managed.
 
> In Radium V2, allow to build textures from already defined data instead of an external file. 
Also allow to generate procedural textures (needed by some glTF extensions)

#### Samplers
Sampler are used to parameterize the Radium textures. Due to limitations in texture management in Radium, there are 
some inconsistencies in the sampler management.
When a texture is referenced through several samplers, only the first will be used for all the instances.

### Materials
Physically based rendering is supported in Radium. glTF materials are fully supported if they belong to the core specification. 
Adding support for other materials might be done easily as soon as there specification and json schema are known.
 
#### Metallic-Roughness Material 
This material is integrated in the glTF plugin.

#### Additional Maps
The main texture maps defined by the specification are integrated.

#### Alpha Coverage
Alpha coverage test succeed as soon as there is no sampler collision (see above)

#### Double Sided
Double sided materials are manage directly in the shader by discarding (or not) fragments that are back-side.

#### Point and Line Materials
Not yet supported.

### Cameras
Camera are loaded and transmitted to Radium.

#### Projection Matrices
Both perspective and orthogonal projection matrices are managed

### Animations
Animation key-framed definition is properly loaded and transmitted to Radium. Note that all interpolators are transmitted to Radium as the linear one.

> In a next version, support all the key-frames interpolators.

### Specifying Extensions
glTF defines an extension mechanism that allows the base format to be extended with new capabilities. 
Any glTF object can have an optional `extensions` property, as in the following example:

#### Supported extensions
* __KHR_materials_pbrSpecularGlossiness__, that define a PBR material based on specular color and 
shininess exponent is supported (spec at https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_pbrSpecularGlossiness).
  
* __KHR_texture_transform__, that allow to apply transformation on texture coordinate is supported 
  (spec at https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_texture_transform)
