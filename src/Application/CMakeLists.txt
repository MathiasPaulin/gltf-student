cmake_minimum_required(VERSION 3.6)
#-------------------------------------------------------------------------------
# mkdir build && cd build
# cmake -DRadium_DIR=/pathToInstalledRadium/Bundle-GNU/lib/cmake/Radium/
#       -DRadiumGlTF_DIR=/pasToInstalllibGLTF/lib/cmake ..
#
# cmake -DRadium_DIR=/Users/mathias/Professionnel/SVN-IRIT-paulin/paulin/Developpement/Radium/Radium-Engine/Bundle-GNU/lib/cmake/Radium/ -DCMAKE_CXX_COMPILER=/opt/local/bin/g++ -DCMAKE_C_COMPILER=/opt/local/bin/gcc -DRadiumGlTF_DIR=/Users/mathias/Professionnel/SVN-IRIT-paulin/paulin/Developpement/Radium/InstalledLibsAndPlugins/lib/cmake ..

# exampleApp executables setup
project(gltfDemo)

# Configure the application target
set(app_sources
        main.cpp
        RadiumApplication.cpp RadiumApplication.hpp
        )

add_executable(${PROJECT_NAME} ${app_sources})
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
target_include_directories(${PROJECT_NAME} PRIVATE
        externals/
        ${CMAKE_CURRENT_SOURCE_DIR}
        )

#------------------------------------------------------------------------------
# Application specific

if (GLTFDEMO_IN_BUILD_TREE)
    message(STATUS "-- Building application into the GLTF library build tree")
    find_package(Radium REQUIRED Core Engine)
else ()
    message(STATUS "-- Building application from the installed GLTF library")
    find_package(RadiumGlTF REQUIRED)
endif ()
target_link_libraries(${PROJECT_NAME} PUBLIC Radium::Core Radium::Engine RadiumGlTF::RadiumGlTF)

# call the installation configuration (defined in RadiumConfig.cmake)
configure_radium_app(
        NAME ${PROJECT_NAME}
)


