#include "RadiumApplication.hpp"

#include <Core/Containers/MakeShared.hpp>
#include <Core/Geometry/TopologicalMesh.hpp>
#include <Core/Utils/Log.hpp>
#include <Engine/Data/MaterialConverters.hpp>
#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/MetallicRoughnessMaterialConverter.hpp>
#include <Engine/RadiumEngine.hpp>
#include <Engine/Rendering/RenderObject.hpp>
#include <Engine/Rendering/RenderObjectManager.hpp>
#include <Engine/Scene/EntityManager.hpp>
#include <Engine/Scene/GeometrySystem.hpp>
#include <Engine/Scene/SystemDisplay.hpp>
#include <IO/Gltf/Loader/glTFFileLoader.hpp>
#include <IO/Gltf/Writer/glTFFileWriter.hpp>


using namespace Ra::Core::Utils; // log

GLTFInOutApplication::GLTFInOutApplication() {
    cmdline_parser.add_option( "-o,--output", output_file, "output file." )->required();
    cmdline_parser.add_option( "-t,--texture-prefix", texture_prefix, "texture uri prefix." )
        ->default_str( "" );
}

GLTFInOutApplication::~GLTFInOutApplication() {
    if ( m_engine ) { m_engine->cleanup(); }
};

int GLTFInOutApplication::init( int argc, const char* argv[] ) {
    int c = 0;
    try
    { cmdline_parser.parse( argc, argv ); }
    catch ( const CLI::ParseError& e )
    {
        c = cmdline_parser.exit( e ) + 1;
        return c;
    }
    // Create engine
    m_engine.reset( Ra::Engine::RadiumEngine::createInstance() );
    m_engine->initialize();

    // Register the GeometrySystem converting loaded assets to meshes
    m_engine->registerSystem( "GeometrySystem", new Ra::Engine::Scene::GeometrySystem, 1000 );
    // register the gltf loader
    std::shared_ptr<Ra::Core::Asset::FileLoaderInterface> loader =
        std::make_shared<GLTF::glTFFileLoader>();
    m_engine->registerFileLoader( loader );
    // register gltf to radium material converters
    Ra::Engine::Data::EngineMaterialConverters::registerMaterialConverter(
        "MetallicRoughness", GLTF::MetallicRoughnessMaterialConverter() );

    auto lastSlash = scene_file.find_last_of( '/' );
    if ( lastSlash != std::string::npos )
    {
        auto prefix = scene_file.substr( 0, lastSlash + 1 );
        output_file = prefix + output_file;
    }

    LOG( logINFO ) << "\tLoad " << scene_file;
    LOG( logINFO ) << "\tSave " << output_file;

    return c;
}

int GLTFInOutApplication::run() {

    // Load the file
    auto loaded = m_engine->loadFile( scene_file );
    if ( !loaded )
    {
        LOG( logERROR ) << "Unable to load " << scene_file;
        return 1;
    }

    // process the scene
    process();

    // save the file
    GLTF::glTFFileWriter writer( output_file, texture_prefix, false );
    writer.write( m_toExport );

    return 0;
}

/* ---------------------------------------------------------------------------------------------- */

void GLTFInOutApplication::process() {
    auto entityManager = Ra::Engine::RadiumEngine::getInstance()->getEntityManager();
    auto entities      = entityManager->getEntities();
    auto roManager     = Ra::Engine::RadiumEngine::getInstance()->getRenderObjectManager();

    for ( const auto e : entities )
    {
        if ( e == Ra::Engine::Scene::SystemEntity::getInstance() ) { continue; }
        m_toExport.push_back( e );
        for ( const auto& c : e->getComponents() )
        {
            for ( const auto& roIdx : c->m_renderObjects )
            {
                const auto& ro = roManager->getRenderObject( roIdx );
                if ( ro->getType() == Ra::Engine::Rendering::RenderObjectType::Geometry )
                {
                    auto theMesh     = ro->getMesh();
                    auto displayMesh = dynamic_cast<Ra::Engine::Data::Mesh*>( theMesh.get() );
                    if ( displayMesh )
                    {
                        LOG( logINFO ) << "\t\tMesh " << displayMesh->getName() << " is processed.";
                        // Bug here !
                        Ra::Core::Geometry::TopologicalMesh topologicalMesh(
                            displayMesh->getCoreGeometry() );
                        LOG( logINFO ) << "\t\tMesh " << displayMesh->getName() << " is updated.";
                        displayMesh->loadGeometry( topologicalMesh.toTriangleMesh() );
                    }
                }
            }
        }
    }
}
