#pragma once
#include <CLI/CLI.hpp>

/**
 * Base class for radium based cmdline application
 *
 */
class RadiumApplication
{

  protected:
    /// The scene file to manage
    std::string scene_file = {""};

    /** Command-line parameters parser.
     * All applications will share parameters --help and --file <scene_file>.
     * Derived classes must extend this parser to add their own parameters
     */
    CLI::App cmdline_parser;

  public:
    /// Base constructor.
    RadiumApplication() {
        cmdline_parser.add_option( "-f,--file", scene_file, "Scene file to process." )
            ->required()
            ->check( CLI::ExistingFile );
    };
    /// Base destructor
    virtual ~RadiumApplication() = default;

    /**
     * Application initialization method.
     *
     * This method is called by the main function to initialize the app giving its parameters.
     * @param argc number of parameter
     * @param argv array of string representing the parameters
     * @return 0 if the application is correctly initialized or an application dependant error code
     * if something went wrong.
     */
    virtual int init( int argc, const char* argv[] ) {
        (void)argc;
        (void)argv;
        return 0;
    }

    /**
     * Run the application.
     *
     * @return 0 if the application was correctly ran or an application dependant error code if
     * something went wrong.
     */
    virtual int run() = 0;
};

/* ************************************************************************************************/

namespace Ra {
namespace Engine {
namespace Scene {
class Entity;
}
class RadiumEngine;
} // namespace Engine
} // namespace Ra

class GLTFInOutApplication : public RadiumApplication
{
  private:
    /// The scene file to manage
    std::string output_file    = {""};
    std::string texture_prefix = {""};

    /// Instance of the radium engine.
    std::unique_ptr<Ra::Engine::RadiumEngine> m_engine;

    /// Entities resulting from the processing
    std::vector<Ra::Engine::Scene::Entity*> m_toExport;

  public:
    GLTFInOutApplication();
    ~GLTFInOutApplication() override;
    int init( int argc, const char* argv[] ) override;
    int run() override;
    virtual void process();
};
