#include "RadiumApplication.hpp"

int main( int argc, const char* argv[] ) {
    GLTFInOutApplication myApp;
    int code;
    if ( ( code = myApp.init( argc, argv ) ) ) { return code; }
    code = myApp.run();
    return code;
}
