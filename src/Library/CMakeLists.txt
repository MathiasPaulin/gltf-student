cmake_minimum_required(VERSION 3.6)
#------------------------------------------------------------------------------
# Policies and global parameters for CMake
if (POLICY CMP0077)
    # allow to define options cache variable before the option is declared
    # https://cmake.org/cmake/help/latest/policy/CMP0077.html
    cmake_policy(SET CMP0077 NEW)
endif ()
if (APPLE)
    # MACOSX_RPATH is enabled by default.
    # https://cmake.org/cmake/help/latest/policy/CMP0042.html
    cmake_policy(SET CMP0042 NEW)
endif (APPLE)
set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

project(RadiumGlTF VERSION 1.0.0)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/installed-${CMAKE_CXX_COMPILER_ID}" CACHE PATH
            "Install path prefix, prepended onto install directories." FORCE)
    message("Set install prefix to ${CMAKE_INSTALL_PREFIX}")
    set(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT False)
endif ()

# Radium and Qt stuff
find_package(Radium REQUIRED Core Engine)

set(markdowns
    #        Readme.md
)

set(sources
    glTFLibrary.cpp
    Engine/Data/GLTFMaterial.cpp
    Engine/Data/MetallicRoughnessMaterialConverter.cpp
    Engine/Data/SpecularGlossinessMaterialConverter.cpp
    Engine/Data/MetallicRoughnessMaterial.cpp
    Engine/Data/SpecularGlossinessMaterial.cpp
    Core/Asset/BaseGLTFMaterial.cpp
    Core/Asset/MetallicRoughnessMaterialData.cpp
    Core/Asset/SpecularGlossinessMaterialData.cpp
    IO/Gltf/Loader/glTFFileLoader.cpp
    IO/Gltf/Loader/GLTFConverter/AccessorReader.cpp
    IO/Gltf/Loader/GLTFConverter/MaterialConverter.cpp
    IO/Gltf/Loader/GLTFConverter/MeshData.cpp
    IO/Gltf/Loader/GLTFConverter/TangentCalculator.cpp
    IO/Gltf/Loader/GLTFConverter/Converter.cpp
    IO/Gltf/Loader/GLTFConverter/HandleData.cpp
    IO/Gltf/Loader/GLTFConverter/NormalCalculator.cpp
    IO/Gltf/Loader/GLTFConverter/SceneNode.cpp
    IO/Gltf/Loader/GLTFConverter/TransformationManager.cpp
    IO/Gltf/Loader/GLTFConverter/mikktspace.c
    IO/Gltf/Writer/glTFFileWriter.cpp
)

set(public_headers
    glTFLibrary.hpp
    glTFLibraryMacros.hpp
    Core/Asset/BaseGLTFMaterial.hpp
    Core/Asset/MetallicRoughnessMaterialData.hpp
    Core/Asset/SpecularGlossinessMaterialData.hpp
    Core/Asset/GLTFTextureParameters.hpp
    Engine/Data/GLTFMaterial.hpp
    Engine/Data/MetallicRoughnessMaterial.hpp
    Engine/Data/MetallicRoughnessMaterialConverter.hpp
    Engine/Data/SpecularGlossinessMaterial.hpp
    Engine/Data/SpecularGlossinessMaterialConverter.hpp
    IO/Gltf/Loader/glTFFileLoader.hpp
    IO/Gltf/Writer/glTFFileWriter.hpp
)

set(headers
    IO/Gltf/Common/Extensions/MaterialExtensions.hpp
    IO/Gltf/Common/Extensions/LightExtensions.hpp
    IO/Gltf/Loader/GLTFConverter/AccessorReader.hpp
    IO/Gltf/Loader/GLTFConverter/HandleData.hpp
    IO/Gltf/Loader/GLTFConverter/TransformationManager.hpp
    IO/Gltf/Loader/GLTFConverter/Converter.hpp
    IO/Gltf/Loader/GLTFConverter/ImageData.hpp
    IO/Gltf/Loader/GLTFConverter/NormalCalculator.hpp
    IO/Gltf/Loader/GLTFConverter/MaterialConverter.hpp
    IO/Gltf/Loader/GLTFConverter/MeshData.hpp
    IO/Gltf/Loader/GLTFConverter/SceneNode.hpp
    IO/Gltf/Loader/GLTFConverter/TangentCalculator.hpp
    IO/Gltf/Loader/GLTFConverter/mikktspace.h
)

set(public_inlines
    Engine/Data//GLTFMaterial.inl
)

set(resources
    Resources/Shaders
)


# Our library project uses these sources and headers.
add_library(
    ${PROJECT_NAME} SHARED
    ${sources}
    ${headers}
    ${public_headers}
    ${public_inlines}
    ${markdowns}
    ${resources}
)

# Include look up directories
target_include_directories(${PROJECT_NAME} PRIVATE
        IO/Gltf/Common/
)

target_compile_definitions(${PROJECT_NAME} PRIVATE USE_FX_GLTF  ${PROJECT_NAME}_EXPORTS LIBRARY_NAME=\"${PROJECT_NAME}\")

target_link_libraries(
        ${PROJECT_NAME}
        PUBLIC
        Radium::Core
        Radium::Engine
)

#-----------------------------------------------------------------------------------

message("Configure library ${PROJECT_NAME} for insertion into Radium exosystem")
configure_radium_library(
        TARGET ${PROJECT_NAME}
        TARGET_DIR ${PROJECT_NAME}
        NAMESPACE ${PROJECT_NAME}
        PACKAGE_DIR ${CMAKE_INSTALL_PREFIX}/lib/cmake
        PACKAGE_CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
        FILES "${public_headers};${public_inlines}"
)

install_target_resources(
        TARGET ${PROJECT_NAME}
        RESOURCES_INSTALL_DIR ${PROJECT_NAME}
        RESOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/Resources/Shaders
)

