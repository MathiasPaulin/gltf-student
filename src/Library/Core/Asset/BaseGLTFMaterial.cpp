#include <Core/Asset/BaseGLTFMaterial.hpp>
namespace GLTF {

BaseGLTFMaterial::BaseGLTFMaterial( const std::string& gltfType, const std::string& instanceName ) :
    Ra::Core::Asset::MaterialData( instanceName, gltfType ) {}

} // namespace GLTF
