#pragma once

#include <Core/Asset/GLTFTextureParameters.hpp>
#include <glTFLibraryMacros.hpp>

// Radium includes
#include <Core/Asset/MaterialData.hpp>
#include <Core/Utils/Color.hpp>

namespace GLTF {

class glTF_LIBRARY_API BaseGLTFMaterial : public Ra::Core::Asset::MaterialData
{
  public:
    /// Attributes of a gltf material
    /// Normal texture
    std::string m_normalTexture {};
    float m_normalTextureScale {1};
    GLTFSampler m_normalSampler {};
    bool m_hasNormalTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_normalTextureTransform {nullptr};

    /// Occlusion texture
    std::string m_occlusionTexture {};
    float m_occlusionStrength {1};
    GLTFSampler m_occlusionSampler {};
    bool m_hasOcclusionTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_occlusionTextureTransform {nullptr};
    /// Emissive texture
    std::string m_emissiveTexture {};
    Ra::Core::Utils::Color m_emissiveFactor {0.0, 0.0, 0.0, 1.0};
    GLTFSampler m_emissiveSampler {};
    bool m_hasEmissiveTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_emissiveTextureTransform {nullptr};

    /// Transparency parameters
    unsigned char m_alphaMode {0};
    float m_alphaCutoff {0.5};

    /// Face culling parameter
    bool m_doubleSided {false};

    // Extension data pass through the system
    gltf_INNMaterialAtlas* m_inn_materialAtlas {nullptr};
    explicit BaseGLTFMaterial( const std::string& gltfType, const std::string& instanceName );
    ~BaseGLTFMaterial() override = default;
};

} // namespace GLTF
