#pragma once
#include <glTFLibraryMacros.hpp>

#include <array>

namespace GLTF {

/**
 * Predeclare gltf_INNMaterialAtlas as this extension will just be kept as raw pointer
 * but not translated
 * @todo is Radium interested by this kind of extension ?
 */
struct gltf_INNMaterialAtlas;

/**
 * Implementation of the gltf_KHRTextureTransform extension.
 *
 */
struct glTF_LIBRARY_API GLTFTextureTransform {
    std::array<Scalar, 2> offset {0_ra, 0_ra};
    std::array<Scalar, 2> scale {1_ra, 1_ra};
    Scalar rotation {0_ra};
    int texCoord {-1};
};

/**
 * Sampler Data as defined by GlTF specification
 * enums correspond to OpenGL specification
 */
struct glTF_LIBRARY_API GLTFSampler {
    enum class MagFilter : uint16_t { Nearest = 9728, Linear = 9729 };

    enum class MinFilter : uint16_t {
        Nearest              = 9728,
        Linear               = 9729,
        NearestMipMapNearest = 9984,
        LinearMipMapNearest  = 9985,
        NearestMipMapLinear  = 9986,
        LinearMipMapLinear   = 9987
    };

    enum class WrappingMode : uint16_t {
        ClampToEdge    = 33071,
        MirroredRepeat = 33648,
        Repeat         = 10497
    };

    MagFilter magFilter {MagFilter::Nearest};
    MinFilter minFilter {MinFilter::Nearest};

    WrappingMode wrapS {WrappingMode::Repeat};
    WrappingMode wrapT {WrappingMode::Repeat};
};

} // namespace GLTF
