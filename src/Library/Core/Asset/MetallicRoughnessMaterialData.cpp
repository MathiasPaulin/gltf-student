#include <Core/Asset/MetallicRoughnessMaterialData.hpp>
namespace GLTF {

MetallicRoughnessData::MetallicRoughnessData( const std::string& name ) :
    BaseGLTFMaterial( {"MetallicRoughness"}, name ) {}

} // namespace GLTF
