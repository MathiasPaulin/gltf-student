#pragma once

#include <Core/Asset/BaseGLTFMaterial.hpp>

namespace GLTF {

/**
 * RadiumIO representation of the material
 */
class glTF_LIBRARY_API MetallicRoughnessData : public BaseGLTFMaterial
{
  public:
    /// Base texture
    std::string m_baseColorTexture {};
    Ra::Core::Utils::Color m_baseColorFactor {1.0, 1.0, 1.0, 1.0};
    GLTFSampler m_baseSampler {};
    bool m_hasBaseColorTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_baseTextureTransform {nullptr};

    /// Metallic-Roughness texture
    std::string m_metallicRoughnessTexture {};
    float m_metallicFactor {1};
    float m_roughnessFactor {1};
    GLTFSampler m_metallicRoughnessSampler {};
    bool m_hasMetallicRoughnessTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_metallicRoughnessTextureTransform {nullptr};

    explicit MetallicRoughnessData( const std::string& name = std::string {} );
    ~MetallicRoughnessData() override = default;
};

} // namespace GLTF
