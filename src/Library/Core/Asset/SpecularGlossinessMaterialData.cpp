#include <Core/Asset/SpecularGlossinessMaterialData.hpp>
namespace GLTF {

SpecularGlossinessData::SpecularGlossinessData( const std::string& name ) :
    BaseGLTFMaterial( {"SpecularGlossiness"}, name ) {}

} // namespace GLTF
