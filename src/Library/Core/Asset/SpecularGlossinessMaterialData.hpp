#pragma once

#include <Core/Asset/BaseGLTFMaterial.hpp>
#include <glTFLibraryMacros.hpp>

namespace GLTF {

/**
 * RadiumIO representation of the material
 */
class glTF_LIBRARY_API SpecularGlossinessData : public BaseGLTFMaterial
{
  public:
    /// Diffuse texture
    std::string m_diffuseTexture {};
    Ra::Core::Utils::Color m_diffuseFactor {1.0, 1.0, 1.0, 1.0};
    GLTFSampler m_diffuseSampler {};
    bool m_hasDiffuseTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_diffuseTextureTransform {nullptr};

    /// Specular-Glossiness texture
    std::string m_specularGlossinessTexture {};
    Ra::Core::Utils::Color m_specularFactor {1.0, 1.0, 1.0, 1.0};
    float m_glossinessFactor {1};
    GLTFSampler m_specularGlossinessSampler {};
    bool m_hasSpecularGlossinessTexture {false};
    mutable std::unique_ptr<GLTFTextureTransform> m_specularGlossinessTransform {nullptr};

    explicit SpecularGlossinessData( const std::string& name = std::string {} );
    ~SpecularGlossinessData() override = default;
};

} // namespace GLTF
