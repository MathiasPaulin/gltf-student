#include <Core/Resources/Resources.hpp>
#include <Engine/Data/GLTFMaterial.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/RadiumEngine.hpp>

namespace GLTF {
using namespace Ra::Engine;

std::string GLTFMaterial::s_shaderBasePath {""};

GLTFMaterial::GLTFMaterial( const std::string& name, const std::string& materialName ) :
    Material( name, materialName, Material::MaterialAspect::MAT_OPAQUE ) {}

GLTFMaterial::~GLTFMaterial() {
    this->m_textures.clear();
}

const GLTFTextureTransform*
GLTFMaterial::getTextureTransform( const TextureSemantic& semantic ) const {
    if ( semantic == "TEX_NORMAL" ) { return m_normalTextureTransform.get(); }
    if ( semantic == "TEX_EMISSIVE" ) { return m_emissiveTextureTransform.get(); }
    if ( semantic == "TEX_OCCLUSION" ) { return m_occlusionTextureTransform.get(); }
    return nullptr;
}

void GLTFMaterial::updateGL() {
    // Load textures
    auto texManager = RadiumEngine::getInstance()->getTextureManager();
    for ( const auto& tex : m_pendingTextures )
    {
        // only manage GLTFMaterial texture semantic.
        if ( tex.first == "TEX_EMISSIVE" )
        { m_textures[tex.first] = texManager->getOrLoadTexture( tex.second, true ); }
        else if ( tex.first == "TEX_NORMAL" || tex.first == "TEX_OCCLUSION" )
        { m_textures[tex.first] = texManager->getOrLoadTexture( tex.second, false ); }
    }

    m_pendingTextures.erase( {"TEX_EMISSIVE"} );
    m_pendingTextures.erase( {"TEX_NORMAL"} );
    m_pendingTextures.erase( {"TEX_OCCLUSION"} );

    m_renderParameters.addParameter( "material.baseMaterial.emissiveFactor", m_emissiveFactor );
    m_renderParameters.addParameter( "material.baseMaterial.alphaMode", m_alphaMode );
    m_renderParameters.addParameter( "material.baseMaterial.alphaCutoff", m_alphaCutoff );
    m_renderParameters.addParameter( "material.baseMaterial.doubleSided", m_doubleSided );

    auto tex = getTexture( {"TEX_NORMAL"} );
    if ( tex != nullptr )
    {
        m_renderParameters.addParameter( "material.baseMaterial.normalTextureScale",
                                         m_normalTextureScale );
        m_renderParameters.addParameter( "material.baseMaterial.normal", tex );
        if ( m_normalTextureTransform )
        {
            auto ct = std::cos( m_normalTextureTransform->rotation );
            auto st = std::sin( m_normalTextureTransform->rotation );
            Ra::Core::Matrix3 tr;
            tr << m_normalTextureTransform->scale[0] * ct, -m_normalTextureTransform->scale[1] * st,
                m_normalTextureTransform->offset[0], m_normalTextureTransform->scale[0] * st,
                m_normalTextureTransform->scale[1] * ct, m_normalTextureTransform->offset[1], 0, 0,
                1;
            m_renderParameters.addParameter( "material.baseMaterial.normalTransform", tr );
        }
    }

    tex = getTexture( {"TEX_OCCLUSION"} );
    if ( tex != nullptr )
    {
        m_renderParameters.addParameter( "material.baseMaterial.occlusionStrength",
                                         m_occlusionStrength );
        m_renderParameters.addParameter( "material.baseMaterial.occlusion", tex );
        if ( m_occlusionTextureTransform )
        {
            auto ct = std::cos( m_occlusionTextureTransform->rotation );
            auto st = std::sin( m_occlusionTextureTransform->rotation );
            Ra::Core::Matrix3 tr;
            tr << m_occlusionTextureTransform->scale[0] * ct,
                -m_occlusionTextureTransform->scale[1] * st, m_occlusionTextureTransform->offset[0],
                m_occlusionTextureTransform->scale[0] * st,
                m_occlusionTextureTransform->scale[1] * ct, m_occlusionTextureTransform->offset[1],
                0, 0, 1;
            m_renderParameters.addParameter( "material.baseMaterial.occlusionTransform", tr );
        }
    }

    tex = getTexture( {"TEX_EMISSIVE"} );
    if ( tex != nullptr )
    {
        m_renderParameters.addParameter( "material.baseMaterial.emissive", tex );
        if ( m_emissiveTextureTransform )
        {
            auto ct = std::cos( m_emissiveTextureTransform->rotation );
            auto st = std::sin( m_emissiveTextureTransform->rotation );
            Ra::Core::Matrix3 tr;
            tr << m_emissiveTextureTransform->scale[0] * ct,
                -m_emissiveTextureTransform->scale[1] * st, m_emissiveTextureTransform->offset[0],
                m_emissiveTextureTransform->scale[0] * st,
                m_emissiveTextureTransform->scale[1] * ct, m_emissiveTextureTransform->offset[1], 0,
                0, 1;
            m_renderParameters.addParameter( "material.baseMaterial.emmissiveTransform", tr );
        }
    }
}

bool GLTFMaterial::isTransparent() const {
    return m_alphaMode == 2;
}

void GLTFMaterial::registerMaterial() {
    // gets the resource path of the plugins
    auto pluginPath = Ra::Core::Resources::getResourcesPath(
        reinterpret_cast<void*>( &GLTFMaterial::registerMaterial ), {"../Resources/"} );
    if ( !pluginPath )
    {
        LOG( Ra::Core::Utils::logERROR ) << "Unable to find resources for Library GLTF";
        return;
    }
    s_shaderBasePath          = *pluginPath + LIBRARY_NAME + "/Shaders";
    auto shaderProgramManager = RadiumEngine::getInstance()->getShaderProgramManager();
    shaderProgramManager->addNamedString( {"/baseGLTFMaterial.glsl"},
                                          s_shaderBasePath + "/Materials/baseGLTFMaterial.glsl" );
}

void GLTFMaterial::fillBaseFrom( const BaseGLTFMaterial* source ) {

    // Warning, must modify this if textures are embedded in the GLTF file
    if ( source->m_hasNormalTexture )
    {
        addTexture( {"TEX_NORMAL"}, source->m_normalTexture, source->m_normalSampler );
        m_normalTextureTransform = std::move( source->m_normalTextureTransform );
    }
    if ( source->m_hasOcclusionTexture )
    {
        addTexture( {"TEX_OCCLUSION"}, source->m_occlusionTexture, source->m_occlusionSampler );
        m_occlusionTextureTransform = std::move( source->m_occlusionTextureTransform );
    }
    if ( source->m_hasEmissiveTexture )
    {
        addTexture( {"TEX_EMISSIVE"}, source->m_emissiveTexture, source->m_emissiveSampler );
        m_emissiveTextureTransform = std::move( source->m_emissiveTextureTransform );
    }
    m_normalTextureScale = source->m_normalTextureScale;
    m_occlusionStrength  = source->m_occlusionStrength;
    m_emissiveFactor     = source->m_emissiveFactor;
    m_alphaMode          = source->m_alphaMode;
    m_alphaCutoff        = source->m_alphaCutoff;
    m_doubleSided        = source->m_doubleSided;
    m_inn_materialAtlas  = source->m_inn_materialAtlas;
}

std::list<std::string> GLTFMaterial::getPropertyList() const {
    std::list<std::string> props = Ra::Engine::Data::Material::getPropertyList();
    // textures
    if ( m_pendingTextures.find( {"TEX_NORMAL"} ) != m_pendingTextures.end() ||
         getTexture( {"TEX_NORMAL"} ) != nullptr )
    {
        props.emplace_back( "TEXTURE_NORMAL" );
        if ( m_normalTextureTransform ) { props.emplace_back( "TEXTURE_COORD_TRANSFORM_NORMAL" ); }
    }
    if ( m_pendingTextures.find( {"TEX_OCCLUSION"} ) != m_pendingTextures.end() ||
         getTexture( {"TEX_OCCLUSION"} ) != nullptr )
    {
        props.emplace_back( "TEXTURE_OCCLUSION" );
        if ( m_occlusionTextureTransform )
        { props.emplace_back( "TEXTURE_COORD_TRANSFORM_OCCLUSION" ); }
    }
    if ( m_pendingTextures.find( {"TEX_EMISSIVE"} ) != m_pendingTextures.end() ||
         getTexture( {"TEX_EMISSIVE"} ) != nullptr )
    {
        props.emplace_back( "TEXTURE_EMISSIVE" );
        if ( m_emissiveTextureTransform )
        { props.emplace_back( "TEXTURE_COORD_TRANSFORM_EMISSIVE" ); }
    }
    return props;
}
} // namespace GLTF
