#pragma once
#include <glTFLibraryMacros.hpp>

#include <Core/Asset/BaseGLTFMaterial.hpp>
#include <Engine/Data/Material.hpp>
#include <Engine/Data/TextureManager.hpp>

namespace Ra::Engine {
class Texture;
} // namespace Ra::Engine

namespace GLTF {
// glTF2-Plugin
#ifndef LIBRARY_NAME
#    define LIBRARY_NAME "glTF2"
#endif

/**
 * Predeclare gltf_INNMaterialAtlas as this extension will just be kept as raw pointer
 * but not translated
 * @todo is Radium interested by this kind of extension ?
 */
struct gltf_INNMaterialAtlas;

/**
 * Radium Engine material representation of pbrMetallicRoughness
 *
 */
class glTF_LIBRARY_API GLTFMaterial : public Ra::Engine::Data::Material
{
  public:
    /** Texture semantics allowed for this material
     *  GLTFMaterial manage the following semantics :
     *    "TEX_NORMAL"
     *    "TEX_OCCLUSION"
     *    "TEX_EMISSIVE"
     */
    using TextureSemantic = std::string;

  public:
    /**
     * Constructor of a named material
     * @param name
     */
    explicit GLTFMaterial( const std::string& name, const std::string& materialName );

    /**
     * Destructor
     */
    ~GLTFMaterial() override;

    /**
     * Add a texture to the material for the given semantic
     * @param semantic
     * @param texture
     * @param sampler
     * @return
     */
    inline Ra::Engine::Data::TextureParameters& addTexture( const TextureSemantic& semantic,
                                                            const std::string& texture,
                                                            const GLTFSampler& sampler );

    /**
     * Get the Radium texture associated with the given semantic
     * @param semantic
     * @return
     */
    [[nodiscard]] inline Ra::Engine::Data::Texture*
    getTexture( const TextureSemantic& semantic ) const;

    /**
     * Get the Radium texture (parameter struct) associated with the given semantic
     * @param semantic
     * @return The TextureParameter description
     */
    [[nodiscard]] inline std::shared_ptr<Ra::Engine::Data::TextureParameters>
    getTextureParameter( const TextureSemantic& semantic ) const;

    /**
     * Get the texture transform associated with the given semantic
     * @param semantic
     * @return a raw pointer to the texture transform, nullptr if thereis no transformation.
     * @note ownership is kept by the GLTFMaterial
     */
    [[nodiscard]] virtual const GLTFTextureTransform*
    getTextureTransform( const TextureSemantic& semantic ) const;

    /**
     * Update the OpenGL component of the material
     */
    void updateGL() override;

    /**
     *
     * @return true if the material is transperent. Depends on the material parameters
     */
    [[nodiscard]] bool isTransparent() const override;

    /**
     * Get the list of properties the material migh use in a shader.
     */
    [[nodiscard]] std::list<std::string> getPropertyList() const override;

    /**
     * Register the material to the Radium Material subsystem
     */
    static void registerMaterial();

    /**
     * Initialize from a BaseGLTFMaterial after reading
     */
    void fillBaseFrom( const BaseGLTFMaterial* source );

    /**
     * GLTF extension, not managed by Radium but kept here for load/save compatibility
     * @todo find a way to not store it as public ?
     */
    gltf_INNMaterialAtlas* m_inn_materialAtlas {nullptr};

    /******************************************************************/
    float getNormalTextureScale() const { return m_normalTextureScale; }
    void setNormalTextureScale( float normalTextureScale ) {
        m_normalTextureScale = normalTextureScale;
    }

    float getOcclusionStrength() const { return m_occlusionStrength; }
    void seOcclusionStrength( float occlusionStrength ) { m_occlusionStrength = occlusionStrength; }

    const Ra::Core::Utils::Color& getEmissiveFactor() const { return m_emissiveFactor; }
    void setEmissiveFactor( const Ra::Core::Utils::Color& emissiveFactor ) {
        m_emissiveFactor = emissiveFactor;
    }

    unsigned char getAlphaMode() const { return m_alphaMode; }
    void setAlphaMode( unsigned char alphaMode ) { m_alphaMode = alphaMode; }

    float getAlphaCutoff() const { return m_alphaCutoff; }
    void setAlphaCutoff( float alphaCutoff ) { m_alphaCutoff = alphaCutoff; }

    bool isDoubleSided() const { return m_doubleSided; }
    void setDoubleSided( bool doubleSided ) { m_doubleSided = doubleSided; }
    /******************************************************************/

  protected:
    inline Ra::Engine::Data::TextureParameters&
    addTexture( const TextureSemantic& type, const Ra::Engine::Data::TextureParameters& texture );
    inline void addTexture( const TextureSemantic& type, Ra::Engine::Data::Texture* texture );

    float m_normalTextureScale {1};
    float m_occlusionStrength {1};
    Ra::Core::Utils::Color m_emissiveFactor {0.0, 0.0, 0.0, 1.0};
    unsigned char m_alphaMode {0};
    float m_alphaCutoff {0.5};
    bool m_doubleSided {false};

    std::map<TextureSemantic, Ra::Engine::Data::Texture*> m_textures;
    std::map<TextureSemantic, Ra::Engine::Data::TextureParameters> m_pendingTextures;

    std::unique_ptr<GLTFTextureTransform> m_normalTextureTransform {nullptr};
    std::unique_ptr<GLTFTextureTransform> m_occlusionTextureTransform {nullptr};
    std::unique_ptr<GLTFTextureTransform> m_emissiveTextureTransform {nullptr};

    static std::string s_shaderBasePath;
};

} // namespace GLTF

#include "GLTFMaterial.inl"
