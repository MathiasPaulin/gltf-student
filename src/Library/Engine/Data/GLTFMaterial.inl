#pragma once
#include <Engine/Data/MetallicRoughnessMaterial.hpp>
#include <string>

namespace GLTF {

inline void GLTFMaterial::addTexture( const TextureSemantic& type,
                                      Ra::Engine::Data::Texture* texture ) {
    m_textures[type] = texture;
    m_pendingTextures.erase( type );
}

inline Ra::Engine::Data::TextureParameters&
GLTFMaterial::addTexture( const TextureSemantic& semantic,
                          const std::string& texture,
                          const GLTFSampler& sampler ) {
    CORE_ASSERT( !texture.empty(), "Invalid texture name" );

    Ra::Engine::Data::TextureParameters textureParams;
    textureParams.name = texture;
    switch ( sampler.wrapS )
    {
    case GLTFSampler::WrappingMode::Repeat:
        textureParams.wrapS = gl::GL_REPEAT;
        break;
    case GLTFSampler::WrappingMode::MirroredRepeat:
        textureParams.wrapS = gl::GL_MIRRORED_REPEAT;
        break;
    case GLTFSampler::WrappingMode::ClampToEdge:
        textureParams.wrapS = gl::GL_CLAMP_TO_EDGE;
        break;
    }
    switch ( sampler.wrapT )
    {
    case GLTFSampler::WrappingMode::Repeat:
        textureParams.wrapT = gl::GL_REPEAT;
        break;
    case GLTFSampler::WrappingMode::MirroredRepeat:
        textureParams.wrapT = gl::GL_MIRRORED_REPEAT;
        break;
    case GLTFSampler::WrappingMode::ClampToEdge:
        textureParams.wrapT = gl::GL_CLAMP_TO_EDGE;
        break;
    }
    switch ( sampler.magFilter )
    {
    case GLTFSampler::MagFilter::Nearest:
        textureParams.magFilter = gl::GL_NEAREST;
        break;
    case GLTFSampler::MagFilter::Linear:
        textureParams.magFilter = gl::GL_LINEAR;
        break;
    }
    switch ( sampler.minFilter )
    {
    case GLTFSampler::MinFilter::Nearest:
        textureParams.minFilter = gl::GL_NEAREST;
        break;
    case GLTFSampler::MinFilter::Linear:
        textureParams.minFilter = gl::GL_LINEAR;
        break;
    case GLTFSampler::MinFilter::NearestMipMapNearest:
        textureParams.minFilter = gl::GL_NEAREST_MIPMAP_NEAREST;
        break;
    case GLTFSampler::MinFilter::LinearMipMapNearest:
        textureParams.minFilter = gl::GL_LINEAR_MIPMAP_NEAREST;
        break;
    case GLTFSampler::MinFilter::NearestMipMapLinear:
        textureParams.minFilter = gl::GL_NEAREST_MIPMAP_LINEAR;
        break;
    case GLTFSampler::MinFilter::LinearMipMapLinear:
        textureParams.minFilter = gl::GL_LINEAR_MIPMAP_LINEAR;
        break;
    }

    return addTexture( semantic, textureParams );
}

inline Ra::Engine::Data::TextureParameters&
GLTFMaterial::addTexture( const TextureSemantic& type,
                          const Ra::Engine::Data::TextureParameters& texture ) {
    m_pendingTextures[type] = texture;
    m_isDirty               = true;

    return m_pendingTextures[type];
}

inline Ra::Engine::Data::Texture*
GLTFMaterial::getTexture( const TextureSemantic& semantic ) const {
    Ra::Engine::Data::Texture* tex = nullptr;

    auto it = m_textures.find( semantic );
    if ( it != m_textures.end() ) { tex = it->second; }

    return tex;
}

inline std::shared_ptr<Ra::Engine::Data::TextureParameters>
GLTFMaterial::getTextureParameter( const TextureSemantic& semantic ) const {
    Ra::Engine::Data::Texture* tex = getTexture( semantic );
    if ( tex == nullptr )
    {
        auto it = m_pendingTextures.find( semantic );
        if ( it != m_pendingTextures.end() )
        { return std::make_shared<Ra::Engine::Data::TextureParameters>( it->second ); }
    }
    else
    { return std::make_shared<Ra::Engine::Data::TextureParameters>( tex->getParameters() ); }
    return nullptr;
}
} // namespace GLTF
