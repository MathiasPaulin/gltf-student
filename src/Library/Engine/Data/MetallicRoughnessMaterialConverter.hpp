#pragma once
#include <glTFLibraryMacros.hpp>

#include <Core/Asset/MaterialData.hpp>
#include <Engine/Data/Material.hpp>

namespace GLTF {
/**
 * Radium IO to Engine conversion for pbrMetallicRoughness
 */
class glTF_LIBRARY_API MetallicRoughnessMaterialConverter
{
  public:
    MetallicRoughnessMaterialConverter() = default;

    ~MetallicRoughnessMaterialConverter() = default;

    Ra::Engine::Data::Material* operator()( const Ra::Core::Asset::MaterialData* toconvert );
};

} // namespace GLTF
