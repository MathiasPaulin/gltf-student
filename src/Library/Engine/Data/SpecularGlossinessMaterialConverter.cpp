#include <Core/Asset/SpecularGlossinessMaterialData.hpp>
#include <Engine/Data/SpecularGlossinessMaterial.hpp>
#include <Engine/Data/SpecularGlossinessMaterialConverter.hpp>

namespace GLTF {

using namespace Ra::Core::Asset;
using namespace Ra::Engine;
using namespace Ra::Engine::Data;

// TODO : make a reusable converter for the commn GLTFMaterial components
Material* SpecularGlossinessMaterialConverter::operator()( const MaterialData* toconvert ) {
    auto result = new SpecularGlossiness( toconvert->getName() );
    auto source = static_cast<const SpecularGlossinessData*>( toconvert );

    result->fillBaseFrom( source );

    result->m_diffuseFactor = source->m_diffuseFactor;
    if ( source->m_hasDiffuseTexture )
    {
        result->addTexture( {"TEX_DIFFUSE"}, source->m_diffuseTexture, source->m_diffuseSampler );
        result->m_diffuseTextureTransform = std::move( source->m_diffuseTextureTransform );
    }
    result->m_specularFactor   = source->m_specularFactor;
    result->m_glossinessFactor = source->m_glossinessFactor;
    if ( source->m_hasSpecularGlossinessTexture )
    {
        result->addTexture( {"TEX_SPECULARGLOSSINESS"},
                            source->m_specularGlossinessTexture,
                            source->m_specularGlossinessSampler );
        result->m_specularGlossinessTransform = std::move( source->m_specularGlossinessTransform );
    }

    return result;
}

} // namespace GLTF
