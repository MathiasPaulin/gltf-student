#pragma once
#include <glTFLibraryMacros.hpp>

#include <Core/Asset/MaterialData.hpp>
#include <Engine/Data/Material.hpp>

namespace GLTF {
/**
 * Radium IO to Engine conversion for pbrSpecularGlossiness
 */
class glTF_LIBRARY_API SpecularGlossinessMaterialConverter
{
  public:
    SpecularGlossinessMaterialConverter() = default;

    ~SpecularGlossinessMaterialConverter() = default;

    Ra::Engine::Data::Material* operator()( const Ra::Core::Asset::MaterialData* toconvert );
};

} // namespace GLTF
