#pragma once
#include <fx/gltf.h>

/**
 * Add default values to the namespace containing all default values.
 * @note : it is not a good idea to add to external namespace
 */
namespace fx::gltf::defaults {
constexpr std::array<float, 2> IdentityVec2 {1, 1};
constexpr std::array<float, 2> NullVec2 {0, 0};
} // namespace fx::gltf::defaults

namespace GLTF {

/**
 * https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_materials_pbrSpecularGlossiness
 *
 */
struct gltf_PBRSpecularGlossiness {
    std::array<float, 4> diffuseFactor {fx::gltf::defaults::IdentityVec4};
    fx::gltf::Material::Texture diffuseTexture;

    float glossinessFactor {fx::gltf::defaults::IdentityScalar};
    std::array<float, 3> specularFactor {fx::gltf::defaults::IdentityVec3};
    fx::gltf::Material::Texture specularGlossinessTexture;

    nlohmann::json extensionsAndExtras {};

    [[nodiscard]] bool empty() const {
        return diffuseTexture.empty() && diffuseFactor == fx::gltf::defaults::IdentityVec4 &&
               glossinessFactor == fx::gltf::defaults::IdentityScalar &&
               specularFactor == fx::gltf::defaults::IdentityVec3 &&
               specularGlossinessTexture.empty();
    }
};

inline void from_json( nlohmann::json const& json,
                       gltf_PBRSpecularGlossiness& pbrSpecularGlossiness ) {
    fx::gltf::detail::ReadOptionalField(
        "diffuseFactor", json, pbrSpecularGlossiness.diffuseFactor );
    fx::gltf::detail::ReadOptionalField(
        "diffuseTexture", json, pbrSpecularGlossiness.diffuseTexture );
    fx::gltf::detail::ReadOptionalField(
        "glossinessFactor", json, pbrSpecularGlossiness.glossinessFactor );
    fx::gltf::detail::ReadOptionalField(
        "specularFactor", json, pbrSpecularGlossiness.specularFactor );
    fx::gltf::detail::ReadOptionalField(
        "specularGlossinessTexture", json, pbrSpecularGlossiness.specularGlossinessTexture );

    fx::gltf::detail::ReadExtensionsAndExtras( json, pbrSpecularGlossiness.extensionsAndExtras );
}

inline void to_json( nlohmann::json& json,
                     gltf_PBRSpecularGlossiness const& pbrSpecularGlossiness ) {
    fx::gltf::detail::WriteField( {"diffuseFactor"},
                                  json,
                                  pbrSpecularGlossiness.diffuseFactor,
                                  fx::gltf::defaults::IdentityVec4 );
    fx::gltf::detail::WriteField( {"diffuseTexture"}, json, pbrSpecularGlossiness.diffuseTexture );
    fx::gltf::detail::WriteField( {"glossinessFactor"},
                                  json,
                                  pbrSpecularGlossiness.glossinessFactor,
                                  fx::gltf::defaults::IdentityScalar );
    fx::gltf::detail::WriteField( {"specularFactor"},
                                  json,
                                  pbrSpecularGlossiness.specularFactor,
                                  fx::gltf::defaults::IdentityVec3 );
    fx::gltf::detail::WriteField(
        {"specularGlossinessTexture"}, json, pbrSpecularGlossiness.specularGlossinessTexture );
    fx::gltf::detail::WriteExtensions( json, pbrSpecularGlossiness.extensionsAndExtras );
}

/**
 * https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_texture_transform
 * KHR_texture_transform
 */
struct gltf_KHRTextureTransform {
    std::array<float, 2> offset {fx::gltf::defaults::NullVec2};
    std::array<float, 2> scale {fx::gltf::defaults::IdentityVec2};
    float rotation {0.0};
    int texCoord {-1};
    nlohmann::json extensionsAndExtras {};

    bool isDefault() {
        return ( rotation == 0.0 ) && ( texCoord == -1 ) &&
               ( offset == fx::gltf::defaults::NullVec2 ) &&
               ( scale == fx::gltf::defaults::IdentityVec2 ) && extensionsAndExtras.is_null();
    }
};

inline void from_json( nlohmann::json const& json, gltf_KHRTextureTransform& khrTextureTransform ) {
    fx::gltf::detail::ReadOptionalField( "offset", json, khrTextureTransform.offset );
    fx::gltf::detail::ReadOptionalField( "scale", json, khrTextureTransform.scale );
    fx::gltf::detail::ReadOptionalField( "rotation", json, khrTextureTransform.rotation );
    fx::gltf::detail::ReadOptionalField( "texCoord", json, khrTextureTransform.texCoord );

    fx::gltf::detail::ReadExtensionsAndExtras( json, khrTextureTransform.extensionsAndExtras );
}

inline void to_json( nlohmann::json& json, gltf_KHRTextureTransform const& khrTextureTransform ) {
    fx::gltf::detail::WriteField(
        {"offset"}, json, khrTextureTransform.offset, fx::gltf::defaults::NullVec2 );
    fx::gltf::detail::WriteField(
        {"scale"}, json, khrTextureTransform.scale, fx::gltf::defaults::IdentityVec2 );
    fx::gltf::detail::WriteField( {"rotation"}, json, khrTextureTransform.rotation, 0.f );
    fx::gltf::detail::WriteField( {"texCoord"}, json, khrTextureTransform.texCoord, -1 );

    fx::gltf::detail::WriteExtensions( json, khrTextureTransform.extensionsAndExtras );
}

/**
 * INN_material_atlas_V1
 *
 */
struct gltf_INNMaterialAtlas {
    struct INN_AtlasTexture : fx::gltf::Material::Texture {
        int nbMaterial {0};
    };

    INN_AtlasTexture atlasTexture;
};

inline void from_json( nlohmann::json const& json,
                       gltf_INNMaterialAtlas::INN_AtlasTexture& atlasTexture ) {
    from_json( json, static_cast<fx::gltf::Material::Texture&>( atlasTexture ) );
    fx::gltf::detail::ReadRequiredField( "nbMaterial", json, atlasTexture.nbMaterial );
}

inline void from_json( nlohmann::json const& json, gltf_INNMaterialAtlas& textureAtlas ) {
    fx::gltf::detail::ReadRequiredField( "atlasTexture", json, textureAtlas.atlasTexture );
}

inline void to_json( nlohmann::json& json,
                     gltf_INNMaterialAtlas::INN_AtlasTexture const& atlasTexture ) {
    to_json( json, static_cast<fx::gltf::Material::Texture const&>( atlasTexture ) );
    fx::gltf::detail::WriteField( {"nbMaterial"}, json, atlasTexture.nbMaterial, 0 );
}

inline void to_json( nlohmann::json& json, gltf_INNMaterialAtlas const& textureAtlas ) {
    fx::gltf::detail::WriteField( {"atlasTexture"}, json, textureAtlas.atlasTexture );
}

} // namespace GLTF
