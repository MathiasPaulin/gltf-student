#include <Core/Asset/MetallicRoughnessMaterialData.hpp>
#include <Core/Asset/SpecularGlossinessMaterialData.hpp>
#include <Core/Utils/Log.hpp>
#include <IO/Gltf/Common/Extensions/MaterialExtensions.hpp>
#include <IO/Gltf/Loader/GLTFConverter/ImageData.hpp>
#include <IO/Gltf/Loader/GLTFConverter/MaterialConverter.hpp>

using namespace fx;

namespace GLTF {
using namespace Ra::Core::Asset;
using namespace Ra::Core::Utils;

GLTFSampler convertSampler( const fx::gltf::Sampler& sampler ) {
    GLTFSampler rasampler;
    switch ( sampler.magFilter )
    {
    case fx::gltf::Sampler::MagFilter::Nearest:
        rasampler.magFilter = GLTFSampler::MagFilter::Nearest;
        break;
    case fx::gltf::Sampler::MagFilter::Linear:
        rasampler.magFilter = GLTFSampler::MagFilter::Linear;
        break;
    default:
        rasampler.magFilter = GLTFSampler::MagFilter::Nearest;
        break;
    }
    switch ( sampler.minFilter )
    {
    case fx::gltf::Sampler::MinFilter::Nearest:
        rasampler.minFilter = GLTFSampler::MinFilter::Nearest;
        break;
    case fx::gltf::Sampler::MinFilter::Linear:
        rasampler.minFilter = GLTFSampler::MinFilter::Linear;
        break;
    case fx::gltf::Sampler::MinFilter::NearestMipMapNearest:
        rasampler.minFilter = GLTFSampler::MinFilter::NearestMipMapNearest;
        break;
    case fx::gltf::Sampler::MinFilter::LinearMipMapNearest:
        rasampler.minFilter = GLTFSampler::MinFilter::LinearMipMapNearest;
        break;
    case fx::gltf::Sampler::MinFilter::NearestMipMapLinear:
        rasampler.minFilter = GLTFSampler::MinFilter::NearestMipMapLinear;
        break;
    case fx::gltf::Sampler::MinFilter::LinearMipMapLinear:
        rasampler.minFilter = GLTFSampler::MinFilter::LinearMipMapLinear;
        break;
    default:
        rasampler.minFilter = GLTFSampler::MinFilter::Nearest;
        break;
    }
    switch ( sampler.wrapS )
    {
    case fx::gltf::Sampler::WrappingMode::ClampToEdge:
        rasampler.wrapS = GLTFSampler::WrappingMode ::ClampToEdge;
        break;
    case fx::gltf::Sampler::WrappingMode::MirroredRepeat:
        rasampler.wrapS = GLTFSampler::WrappingMode ::MirroredRepeat;
        break;
    case fx::gltf::Sampler::WrappingMode::Repeat:
        rasampler.wrapS = GLTFSampler::WrappingMode ::Repeat;
        break;
    }
    switch ( sampler.wrapT )
    {
    case fx::gltf::Sampler::WrappingMode::ClampToEdge:
        rasampler.wrapT = GLTFSampler::WrappingMode ::ClampToEdge;
        break;
    case fx::gltf::Sampler::WrappingMode::MirroredRepeat:
        rasampler.wrapT = GLTFSampler::WrappingMode ::MirroredRepeat;
        break;
    case fx::gltf::Sampler::WrappingMode::Repeat:
        rasampler.wrapT = GLTFSampler::WrappingMode ::Repeat;
        break;
    }

    return rasampler;
}

void getMaterialExtensions( const nlohmann::json& extensionsAndExtras, BaseGLTFMaterial* mat ) {
    // Manage material extensions
    if ( !extensionsAndExtras.empty() )
    {
        auto ext = extensionsAndExtras.find( "extensions" );
        if ( ext != extensionsAndExtras.end() )
        {
            auto extensions                           = *ext;
            const nlohmann::json::const_iterator iter = extensions.find( "INN_material_atlas_V1" );
            if ( iter != extensions.end() )
            {
                mat->m_inn_materialAtlas = new gltf_INNMaterialAtlas;
                from_json( *iter, *( mat->m_inn_materialAtlas ) );
            }
        }
    }
}

void getMaterialTextureTransform( const nlohmann::json& extensionsAndExtras,
                                  std::unique_ptr<GLTFTextureTransform>& dest ) {
    if ( !extensionsAndExtras.empty() )
    {
        auto ext = extensionsAndExtras.find( "extensions" );
        if ( ext != extensionsAndExtras.end() )
        {
            nlohmann::json textureExtensions    = *ext;
            nlohmann::json::const_iterator iter = textureExtensions.find( "KHR_texture_transform" );
            if ( iter != textureExtensions.end() )
            {
                gltf_KHRTextureTransform textTransform;
                from_json( *iter, textTransform );
                dest            = std::make_unique<GLTFTextureTransform>();
                dest->offset[0] = textTransform.offset[0];
                dest->offset[1] = textTransform.offset[1];
                dest->scale[0]  = textTransform.scale[0];
                dest->scale[1]  = textTransform.scale[1];
                dest->rotation  = textTransform.rotation;
                dest->texCoord  = textTransform.texCoord;
            }
        }
    }
}

void getCommonMaterialParameters( const gltf::Document& doc,
                                  const std::string& filePath,
                                  const fx::gltf::Material& gltfMaterial,
                                  BaseGLTFMaterial* mat ) {
    // Normal texture
    if ( !gltfMaterial.normalTexture.empty() )
    {
        ImageData tex {doc, gltfMaterial.normalTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_normalTexture      = tex.Info().FileName;
            mat->m_normalTextureScale = gltfMaterial.normalTexture.scale;
            mat->m_hasNormalTexture   = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex = doc.textures[gltfMaterial.normalTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_normalSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform( gltfMaterial.normalTexture.extensionsAndExtras,
                                     mat->m_normalTextureTransform );
    }
    // Occlusion texture
    if ( !gltfMaterial.occlusionTexture.empty() )
    {
        ImageData tex {doc, gltfMaterial.occlusionTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_occlusionTexture    = tex.Info().FileName;
            mat->m_occlusionStrength   = gltfMaterial.occlusionTexture.strength;
            mat->m_hasOcclusionTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex = doc.textures[gltfMaterial.occlusionTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_occlusionSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform( gltfMaterial.occlusionTexture.extensionsAndExtras,
                                     mat->m_occlusionTextureTransform );
    }
    // Emmissive Component
    mat->m_emissiveFactor = {gltfMaterial.emissiveFactor[0],
                             gltfMaterial.emissiveFactor[1],
                             gltfMaterial.emissiveFactor[2],
                             1_ra};
    if ( !gltfMaterial.emissiveTexture.empty() )
    {
        ImageData tex {doc, gltfMaterial.emissiveTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_emissiveTexture    = tex.Info().FileName;
            mat->m_hasEmissiveTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex = doc.textures[gltfMaterial.emissiveTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_emissiveSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform( gltfMaterial.emissiveTexture.extensionsAndExtras,
                                     mat->m_emissiveTextureTransform );
    }
    mat->m_alphaMode   = static_cast<unsigned char>( gltfMaterial.alphaMode );
    mat->m_doubleSided = gltfMaterial.doubleSided;
    mat->m_alphaCutoff = gltfMaterial.alphaCutoff;

    // load supported material extensions
    getMaterialExtensions( gltfMaterial.extensionsAndExtras, mat );
}

Ra::Core::Asset::MaterialData* buildDefaultMaterial( const gltf::Document& doc,
                                                     int32_t meshIndex,
                                                     const std::string& filePath,
                                                     int32_t meshPartNumber,
                                                     const MaterialData& meshMaterial ) {
    auto gltfMaterial = meshMaterial.Data();
    std::string materialName {gltfMaterial.name};
    if ( materialName.empty() ) { materialName = "material_"; }
    materialName += std::to_string( meshIndex ) + "_p_" + std::to_string( meshPartNumber );
    auto mat = new MetallicRoughnessData( materialName );
    getCommonMaterialParameters( doc, filePath, gltfMaterial, mat );
    return mat;
}

Ra::Core::Asset::MaterialData* buildMetallicRoughnessMaterial( const gltf::Document& doc,
                                                               int32_t meshIndex,
                                                               const std::string& filePath,
                                                               int32_t meshPartNumber,
                                                               const MaterialData& meshMaterial ) {
    auto gltfMaterial = meshMaterial.Data();
    std::string materialName {gltfMaterial.name};
    if ( materialName.empty() )
    {
        materialName = "material_";
        materialName += std::to_string( meshIndex ) + "_p_" + std::to_string( meshPartNumber );
    }

    auto mat = new MetallicRoughnessData( materialName );

    getCommonMaterialParameters( doc, filePath, gltfMaterial, mat );

    // Base color Component
    mat->m_baseColorFactor = {gltfMaterial.pbrMetallicRoughness.baseColorFactor[0],
                              gltfMaterial.pbrMetallicRoughness.baseColorFactor[1],
                              gltfMaterial.pbrMetallicRoughness.baseColorFactor[2],
                              gltfMaterial.pbrMetallicRoughness.baseColorFactor[3]};
    if ( !gltfMaterial.pbrMetallicRoughness.baseColorTexture.empty() )
    {
        ImageData tex {doc, gltfMaterial.pbrMetallicRoughness.baseColorTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_baseColorTexture    = tex.Info().FileName;
            mat->m_hasBaseColorTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex =
            doc.textures[gltfMaterial.pbrMetallicRoughness.baseColorTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_baseSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform(
            gltfMaterial.pbrMetallicRoughness.baseColorTexture.extensionsAndExtras,
            mat->m_baseTextureTransform );
    }

    // Metalic-Roughness  Component
    mat->m_metallicFactor  = gltfMaterial.pbrMetallicRoughness.metallicFactor;
    mat->m_roughnessFactor = gltfMaterial.pbrMetallicRoughness.roughnessFactor;
    if ( !gltfMaterial.pbrMetallicRoughness.metallicRoughnessTexture.empty() )
    {
        ImageData tex {
            doc, gltfMaterial.pbrMetallicRoughness.metallicRoughnessTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_metallicRoughnessTexture    = tex.Info().FileName;
            mat->m_hasMetallicRoughnessTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex =
            doc.textures[gltfMaterial.pbrMetallicRoughness.metallicRoughnessTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_metallicRoughnessSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform(
            gltfMaterial.pbrMetallicRoughness.metallicRoughnessTexture.extensionsAndExtras,
            mat->m_metallicRoughnessTextureTransform );
    }
    return mat;
}

Ra::Core::Asset::MaterialData*
buildSpecularGlossinessMaterial( const gltf::Document& doc,
                                 int32_t meshIndex,
                                 const std::string& filePath,
                                 int32_t meshPartNumber,
                                 const MaterialData& meshMaterial,
                                 const gltf_PBRSpecularGlossiness& specularGloss ) {
    auto gltfMaterial = meshMaterial.Data();
    std::string materialName {gltfMaterial.name};
    if ( materialName.empty() ) { materialName = "material_"; }
    materialName += std::to_string( meshIndex ) + "_p_" + std::to_string( meshPartNumber );

    auto mat = new SpecularGlossinessData( materialName );

    getCommonMaterialParameters( doc, filePath, gltfMaterial, mat );

    // Diffuse color Component
    mat->m_diffuseFactor = {specularGloss.diffuseFactor[0],
                            specularGloss.diffuseFactor[1],
                            specularGloss.diffuseFactor[2],
                            specularGloss.diffuseFactor[3]};

    if ( !specularGloss.diffuseTexture.empty() )
    {
        ImageData tex {doc, specularGloss.diffuseTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_diffuseTexture    = tex.Info().FileName;
            mat->m_hasDiffuseTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex = doc.textures[specularGloss.diffuseTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_diffuseSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform( specularGloss.diffuseTexture.extensionsAndExtras,
                                     mat->m_diffuseTextureTransform );
    }

    // Specular-glossiness  Component
    mat->m_glossinessFactor = specularGloss.glossinessFactor;
    mat->m_specularFactor   = {specularGloss.specularFactor[0],
                             specularGloss.specularFactor[1],
                             specularGloss.specularFactor[2],
                             1_ra};
    if ( !specularGloss.specularGlossinessTexture.empty() )
    {
        ImageData tex {doc, specularGloss.specularGlossinessTexture.index, filePath};
        if ( !tex.Info().IsBinary() )
        {
            mat->m_specularGlossinessTexture    = tex.Info().FileName;
            mat->m_hasSpecularGlossinessTexture = true;
        }
        else
        { LOG( logINFO ) << "GLTF converter -- Embeded texture not supported yet"; }
        // get sampler information for this texture
        int samplerIndex = doc.textures[specularGloss.specularGlossinessTexture.index].sampler;
        if ( samplerIndex >= 0 )
        { mat->m_specularGlossinessSampler = convertSampler( doc.samplers[samplerIndex] ); }
        getMaterialTextureTransform( specularGloss.specularGlossinessTexture.extensionsAndExtras,
                                     mat->m_specularGlossinessTransform );
    }
    return mat;
}

Ra::Core::Asset::MaterialData* buildMaterial( const gltf::Document& doc,
                                              int32_t meshIndex,
                                              const std::string& filePath,
                                              int32_t meshPartNumber,
                                              const MaterialData& meshMaterial ) {
    if ( meshMaterial.isMetallicRoughness() )
    {
        return buildMetallicRoughnessMaterial(
            doc, meshIndex, filePath, meshPartNumber, meshMaterial );
    }
    else
    {
        // Check if extension "KHR_materials_pbrSpecularGlossiness" is available
        auto extensionsAndExtras = meshMaterial.Data().extensionsAndExtras;
        if ( !extensionsAndExtras.empty() )
        {
            auto extensions = extensionsAndExtras.find( "extensions" );
            if ( extensions != extensionsAndExtras.end() )
            {
                auto iter = extensions->find( "KHR_materials_pbrSpecularGlossiness" );
                if ( iter != extensions->end() )
                {
                    gltf_PBRSpecularGlossiness gltfMaterial;
                    from_json( *iter, gltfMaterial );
                    return buildSpecularGlossinessMaterial(
                        doc, meshIndex, filePath, meshPartNumber, meshMaterial, gltfMaterial );
                }
            }
        }
        /// TODO : generate a default MetallicRoughness with the base parameters
        return buildDefaultMaterial( doc, meshIndex, filePath, meshPartNumber, meshMaterial );
    }
}

} // namespace GLTF
