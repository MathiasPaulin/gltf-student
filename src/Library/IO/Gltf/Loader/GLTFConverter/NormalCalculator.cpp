#include <Core/Asset/GeometryData.hpp>
#include <IO/Gltf/Loader/GLTFConverter/NormalCalculator.hpp>

namespace GLTF {
using namespace Ra::Core;
using namespace Ra::Core::Asset;

void NormalCalculator::operator()( GeometryData* gdp, bool basic ) {
    const Vector3Array& vertices = gdp->getVertices();
    const VectorNuArray& faces   = gdp->getFaces();
    Vector3Array& normals        = gdp->getNormals();
    normals.clear();
    normals.resize( gdp->getVerticesSize(), Vector3::Zero() );
    for ( const auto& t : faces )
    {
        Vector3 n = getTriangleNormal( t, basic, vertices );
        for ( uint i = 0; i < 3; ++i )
        {
            normals[t[i]] += n;
        }
    }
    normals.getMap().colwise().normalize();
}

Vector3 NormalCalculator::getTriangleNormal( const Vector3ui& t,
                                             bool basic,
                                             const Vector3Array& vertices ) {
    auto p = vertices[t[0]];
    auto q = vertices[t[1]];
    auto r = vertices[t[2]];

    const Vector3 n = ( q - p ).cross( r - p );
    if ( n.isApprox( Vector3::Zero() ) ) { return Vector3::Zero(); }
    if ( basic ) { return ( n.normalized() ); }
    else
    { return ( n * 0.5 ); }
}

} // namespace GLTF
