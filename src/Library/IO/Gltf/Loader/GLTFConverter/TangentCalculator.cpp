#include <Core/Types.hpp>
#include <IO/Gltf/Loader/GLTFConverter/TangentCalculator.hpp>

namespace GLTF {

using namespace Ra::Core;
using namespace Ra::Core::Asset;

// Initialize MikkTSpaceInterface with callbacks and run calculator.
void TangentCalculator::operator()( GeometryData* gdp, bool basic ) {
    SMikkTSpaceInterface iface;
    iface.m_getNumFaces          = getNumFaces;
    iface.m_getNumVerticesOfFace = getNumVerticesOfFace;
    iface.m_getPosition          = getPosition;
    iface.m_getNormal            = getNormal;
    iface.m_getTexCoord          = getTexCoord;
    iface.m_setTSpaceBasic       = basic ? setTSpaceBasic : nullptr;
    iface.m_setTSpace            = basic ? nullptr : setTSpace;

    Vector3Array& tangents = gdp->getTangents();
    tangents.resize( gdp->getVerticesSize() );

    if ( !basic )
    {
        Vector3Array& bitangents = gdp->getBiTangents();
        bitangents.resize( gdp->getVerticesSize() );
    }
    SMikkTSpaceContext context;
    context.m_pInterface = &iface;
    context.m_pUserData  = gdp;

    genTangSpaceDefault( &context );

    // Do we need to renormalize/orthogonalize ?
}

// Return number of primitives in the geometry.
int TangentCalculator::getNumFaces( const SMikkTSpaceContext* context ) {
    // Cast the void pointer from context data to our GU_Detail pointer.
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );
    return int( gdp->getFaces().size() );
}

// Return number of vertices in the primitive given by index.
int TangentCalculator::getNumVerticesOfFace( const SMikkTSpaceContext* /*context*/,
                                             int /*primnum*/ ) {
    return 3;
}

// Write 3-float position of the vertex's point.
void TangentCalculator::getPosition( const SMikkTSpaceContext* context,
                                     float outpos[],
                                     int primnum,
                                     int vtxnum ) {
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );

    auto face   = gdp->getFaces()[primnum];
    auto vertex = gdp->getVertices()[face[vtxnum]];

    // Write into the input 3-float array.
    outpos[0] = vertex[0];
    outpos[1] = vertex[1];
    outpos[2] = vertex[2];
}

// Write 3-float vertex normal.
void TangentCalculator::getNormal( const SMikkTSpaceContext* context,
                                   float outnormal[],
                                   int primnum,
                                   int vtxnum ) {
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );

    auto face   = gdp->getFaces()[primnum];
    auto normal = gdp->getNormals()[face[vtxnum]];

    outnormal[0] = normal[0];
    outnormal[1] = normal[1];
    outnormal[2] = normal[2];
}

// Write 2-float vertex uv.
void TangentCalculator::getTexCoord( const SMikkTSpaceContext* context,
                                     float outuv[],
                                     int primnum,
                                     int vtxnum ) {
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );

    auto face = gdp->getFaces()[primnum];
    auto uv   = gdp->getTexCoords()[face[vtxnum]];

    outuv[0] = uv[0];
    outuv[1] = uv[1];
}

// Compute and set attributes on the geometry vertex. Basic version.
void TangentCalculator::setTSpaceBasic( const SMikkTSpaceContext* context,
                                        const float tangentu[],
                                        float sign,
                                        int primnum,
                                        int vtxnum ) {
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );

    auto face              = gdp->getFaces()[primnum];
    Vector3Array& tangents = gdp->getTangents();
    int tgtindex           = face[vtxnum];

    // well, some liberties against the spec and the MKKTSpace algo ...
    // tangents[tgtindex] = tangents[tgtindex] + Ra::Core::Vector3(tangentu[0], tangentu[1],
    // tangentu[2]) * sign;
    tangents[tgtindex] = Vector3( tangentu[0], tangentu[1], tangentu[2] ) * sign;
}

// Compute and set attributes on the geometry vertex.
void TangentCalculator::setTSpace( const SMikkTSpaceContext* context,
                                   const float tangentu[],
                                   const float tangentv[],
                                   const float magu,
                                   const float magv,
                                   const tbool /*keep*/,
                                   const int primnum,
                                   const int vtxnum ) {
    auto gdp = static_cast<GeometryData*>( context->m_pUserData );

    auto face                = gdp->getFaces()[primnum];
    Vector3Array& tangents   = gdp->getTangents();
    Vector3Array& bitangents = gdp->getBiTangents();
    int tgtindex             = face[vtxnum];

    tangents[tgtindex]   = Vector3( tangentu[0], tangentu[1], tangentu[2] ) * magu;
    bitangents[tgtindex] = Vector3( tangentv[0], tangentv[1], tangentv[2] ) * magv;
}

} // namespace GLTF
