#ifndef METALLICROUGHNESS_GLSL
#define METALLICROUGHNESS_GLSL
#include "baseGLTFMaterial.glsl"

struct Material {
    GLTFCommon baseMaterial;
    vec4 baseColorFactor;
    float metallicFactor;
    float roughnessFactor;
#ifdef TEXTURE_BASECOLOR
    sampler2D baseColor;
#    ifdef TEXTURE_COORD_TRANSFORM_BASECOLOR
    mat3 baseTransform;
#    endif
#endif
#ifdef TEXTURE_METALLICROUGHNESS
    sampler2D metallicRoughness;
#    ifdef TEXTURE_COORD_TRANSFORM_METALLICROUGHNESS
    mat3 metallicRoughnessTransform;
#    endif
#endif
};

vec3 getEmissiveColor( Material material, vec3 textCoord ) {
    return getEmissiveColorCommon( material.baseMaterial, textCoord );
}

vec3 getNormal( Material material, vec3 texCoord, vec3 N, vec3 T, vec3 B ) {
    return getNormalCommon( material.baseMaterial, texCoord, N, T, B );
}

bool toDiscard( Material material, vec4 color ) {
    return toDiscardCommon( material.baseMaterial, color );
}

// Metallic and roughness are packed scalars
// Following GLTF specification,  Roughness is stored in the 'g' channel, metallic is stored in the
// 'b' channel. The 'r' channel is ignored for MetallicRougness by gltf, we use it to store the
// alphaRoughness
vec3 getMetallicRoughnessFactor( Material material, vec2 texCoord ) {
    vec3 mr = vec3( 0, material.roughnessFactor, material.metallicFactor );
#ifdef TEXTURE_METALLICROUGHNESS
#    ifdef TEXTURE_COORD_TRANSFORM_METALLICROUGHNESS
    vec3 ct = material.metallicRoughnessTransform * vec3( texCoord, 1 );
#    else
    vec3 ct = vec3( texCoord, 1 );
#    endif
    vec3 mrsample = texture( material.metallicRoughness, ct.xy ).rgb;
    mr            = mr * mrsample;
#endif
    clamp( mr, vec3( 0, c_MinRoughness, 0 ), vec3( 0, 1, 1 ) );
    mr.r = mr.g * mr.g;
    return mr;
}

vec4 getBaseColor( Material material, vec3 texCoord ) {
#ifdef TEXTURE_BASECOLOR
#    ifdef TEXTURE_COORD_TRANSFORM_BASECOLOR
    vec3 ct = material.baseTransform * vec3( texCoord.xy, 1 );
#    else
    vec3 ct = vec3( texCoord.xy, 1 );
#    endif
    return texture( material.baseColor, ct.xy ) * material.baseColorFactor;
#else
    return material.baseColorFactor;
#endif
}

vec3 diffuseColor( vec4 baseColor, vec3 metalRoughFactor ) {
    vec3 f0   = vec3( 0.04 );
    vec3 diff = baseColor.rgb * ( vec3( 1.0 ) - f0 );
    diff *= 1.0 - metalRoughFactor.b;
    return diff;
}

vec4 getDiffuseColor( Material material, vec3 texCoord ) {
    vec4 dc = getBaseColor( material, texCoord );
    dc.rgb  = diffuseColor( dc, getMetallicRoughnessFactor( material, texCoord.xy ) );
    return dc;
}

vec3 specularColor( vec4 baseColor, vec3 metalRoughFactor ) {
    vec3 f0 = vec3( 0.04 );
    return mix( f0, baseColor.rgb, metalRoughFactor.b );
}

vec3 getSpecularColor( Material material, vec3 texCoord ) {
    vec4 base     = getBaseColor( material, texCoord );
    vec3 roughmet = getMetallicRoughnessFactor( material, texCoord.xy );
    return specularColor( base, roughmet );
}

/**** BSDF SEPARABLE INTERFACE */
/*** Specific implementation for MetallicRoughness material */
int extractBSDFParameters( Material material,
                           vec3 texC,
                           vec3 L,
                           vec3 V,
                           vec3 N,
                           out PBRInfo params ) {
    if ( extractBaseBSDFParameters( material.baseMaterial, texC, L, V, N, params ) == 0 ) return 0;

    // MetallicRoughness specific code
    params.RoughnessesMetal = getMetallicRoughnessFactor( material, texC.xy );
    vec4 base               = getBaseColor( material, texC );
    vec3 spec               = specularColor( base, params.RoughnessesMetal );
    params.reflectance0     = specularEnvR0( spec );
    params.reflectance90    = specularEnvR90( spec );
    params.diffuseColor     = diffuseColor( base, params.RoughnessesMetal );
    params.specularColor    = spec;
    return 1;
}

int getSeparateBSDFComponent( Material material,
                              vec3 texC,
                              vec3 L,
                              vec3 V,
                              vec3 N,
                              out vec3 diffuse,
                              out vec3 specular ) {
    PBRInfo params;
    if ( extractBSDFParameters( material, texC, L, V, N, params ) == 1 )
    {
        diffuse  = diffuseBSDF( params );
        specular = specularBSDF( params );
        return 1;
    }
    else
    { return 0; }
}

float getGGXRoughness( Material material, vec3 texC ) {
    vec3 roughMetal = getMetallicRoughnessFactor( material, texC.xy );
    return pow( roughMetal.r, 0.25 );
}

/*** BSDF interface */
/* This function expect wi and wo are given in the local frame. */
vec3 evaluateBSDF( Material material, vec3 texC, vec3 wi, vec3 wo ) {
    PBRInfo params;
    if ( extractBSDFParameters( material, texC, wi, wo, vec3( 0, 0, 1 ), params ) == 0 )
    { return vec3( 0 ); }
    return evaluateBsdf( params );
}

uniform Material material;

#endif // METALLICROUGHNESS_GLSL
