#ifndef SPECULARGLOSSINESS_GLSL
#define SPECULARGLOSSINESS_GLSL
#include "baseGLTFMaterial.glsl"

struct Material {
    GLTFCommon baseMaterial;
    vec4 diffuseFactor;
    vec4 specularFactor;
    float glossinessFactor;
#ifdef TEXTURE_DIFFUSE
    sampler2D diffuse;
#    ifdef TEXTURE_COORD_TRANSFORM_DIFFUSE
    mat3 diffuseTransform;
#    endif
#endif
#ifdef TEXTURE_SPECULARGLOSSINESS
    sampler2D specularGlossiness;
#    ifdef TEXTURE_COORD_TRANSFORM_SPECULARGLOSSINESS
    mat3 specularGlossinessTransform;
#    endif
#endif
};

vec3 getEmissiveColor( Material material, vec3 textCoord ) {
    return getEmissiveColorCommon( material.baseMaterial, textCoord );
}

vec3 getNormal( Material material, vec3 texCoord, vec3 N, vec3 T, vec3 B ) {
    return getNormalCommon( material.baseMaterial, texCoord, N, T, B );
}

bool toDiscard( Material material, vec4 color ) {
    return toDiscardCommon( material.baseMaterial, color );
}

// Specular and glossiness are packed scalars
// Following GLTF specification,  Specular is stored in the 'rgb' channel, glossiness is stored in
// the 'a' channel. The 'a' channel of the returned color is the computed alphaRoughness of the BSDF
// model
vec4 getSpecularGlossinessFactor( Material material, vec2 texCoord ) {
    vec4 specGloss = vec4( material.specularFactor.rgb, material.glossinessFactor );
#ifdef TEXTURE_SPECULARGLOSSINESS
#    ifdef TEXTURE_COORD_TRANSFORM_SPECULARGLOSSINESS
    vec3 ct = material.specularGlossinessTransform * vec3( texCoord, 1 );
#    else
    vec3 ct = vec3( texCoord, 1 );
#    endif
    specGloss = texture( material.specularGlossiness, ct.xy );
#endif
    // convert glossinesss to the alpha roughness (alpha = (1-glossiness)^2
    specGloss.a = ( 1 - specGloss.a ) * ( 1 - specGloss.a );
    clamp( specGloss.a, c_MinRoughness, 1 );
    return specGloss;
}

vec4 getBaseColor( Material material, vec3 texCoord ) {
#ifdef TEXTURE_DIFFUSE
#    ifdef TEXTURE_COORD_TRANSFORM_DIFFUSE
    vec3 ct = material.diffuseTransform * vec3( texCoord.xy, 1 );
#    else
    vec3 ct = vec3( texCoord.xy, 1 );
#    endif
    return texture( material.diffuse, ct.xy ) * material.diffuseFactor;
#else
    return material.diffuseFactor;
#endif
}

vec3 diffuseColor( vec4 diffuseFactor, vec4 SpecGloss ) {
    float m   = max( SpecGloss.r, SpecGloss.g );
    m         = max( m, SpecGloss.b );
    vec3 diff = diffuseFactor.rgb * ( 1 - m );
    return diff;
}

vec4 getDiffuseColor( Material material, vec3 texCoord ) {
    vec4 dc = getBaseColor( material, texCoord );
    dc.rgb  = diffuseColor( dc, getSpecularGlossinessFactor( material, texCoord.xy ) );
    return dc;
}

vec3 specularColor( vec4 SpecGloss ) {
    return SpecGloss.rgb;
}

vec3 getSpecularColor( Material material, vec3 texCoord ) {
    vec4 specGloss = getSpecularGlossinessFactor( material, texCoord.xy );
    return specularColor( specGloss );
}

/**** BSDF SEPARABLE INTERFACE */
/*** Specific implementation for SpecularGlossiness material */
int extractBSDFParameters( Material material,
                           vec3 texC,
                           vec3 L,
                           vec3 V,
                           vec3 N,
                           out PBRInfo params ) {
    if ( extractBaseBSDFParameters( material.baseMaterial, texC, L, V, N, params ) == 0 ) return 0;

    // SpecularGlossiness specific code
    vec4 diff               = getBaseColor( material, texC );
    vec4 specGloss          = getSpecularGlossinessFactor( material, texC.xy );
    vec3 spec               = specularColor( specGloss );
    vec3 gloss              = vec3( specGloss.a, sqrt( specGloss.a ), 1 );
    params.RoughnessesMetal = gloss;
    params.reflectance0     = specularEnvR0( spec );
    params.reflectance90    = specularEnvR90( spec );
    params.diffuseColor     = diffuseColor( diff, specGloss );
    params.specularColor    = spec;
    return 1;
}

int getSeparateBSDFComponent( Material material,
                              vec3 texC,
                              vec3 L,
                              vec3 V,
                              vec3 N,
                              out vec3 diffuse,
                              out vec3 specular ) {
    PBRInfo params;
    if ( extractBSDFParameters( material, texC, L, V, N, params ) == 1 )
    {
        diffuse  = diffuseBSDF( params );
        specular = specularBSDF( params );
        return 1;
    }
    else
    { return 0; }
}

float getGGXRoughness( Material material, vec3 texC ) {
    vec4 specRough = getSpecularGlossinessFactor( material, texC.xy );
    return pow( specRough.a, 0.25 );
}

/*** BSDF interface */
/* This function expect wi and wo are given in the local frame. */
vec3 evaluateBSDF( Material material, vec3 texC, vec3 wi, vec3 wo ) {
    PBRInfo params;
    if ( extractBSDFParameters( material, texC, wi, wo, vec3( 0, 0, 1 ), params ) == 0 )
    { return vec3( 0 ); }
    return evaluateBsdf( params );
}
uniform Material material;

#endif // SPECULARGLOSSINESS_GLSL
