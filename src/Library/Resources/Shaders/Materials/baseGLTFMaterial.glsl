#ifndef GLTF_BASEMATERIAL_GLSL
#define GLTF_BASEMATERIAL_GLSL

/*
 *  TODO : update this file from the reference gltf implementation of Chronos
 * https://github.com/KhronosGroup/glTF-Sample-Viewer/tree/master/source/Renderer/shaders
 */


#define USE_CREATOR_ALPHA
/*
 * Define constant, datastructure and operators used for all pbr materials of a GLTF scene
 */
const float M_PI  = 3.141592653589793;
const float InvPi = 0.31830988618379067154;

const float c_MinRoughness = 0.04;

struct GLTFCommon {
    vec4 emissiveFactor;
    int alphaMode;
    int doubleSided;
    float alphaCutoff;
#ifdef TEXTURE_NORMAL
    float normalTextureScale;
    sampler2D normal;
#    ifdef TEXTURE_COORD_TRANSFORM_NORMAL
    mat3 normalTransform;
#    endif
#endif
#ifdef TEXTURE_OCCLUSION
    float occlusionStrength;
    sampler2D occlusion;
#    ifdef TEXTURE_COORD_TRANSFORM_OCCLUSION
    mat3 occlusionTransform;
#    endif
#endif
#ifdef TEXTURE_EMISSIVE
    sampler2D emissive;
#    ifdef TEXTURE_COORD_TRANSFORM_EMISSIVE
    mat3 emmissiveTransform;
#    endif
#endif
};

// Encapsulate the various inputs used by the various functions in the shading equation
// We store values in this struct to simplify the integration of alternative implementations
// of the shading terms, outlined in the Readme.MD Appendix.
struct PBRInfo {
    float NdotL; // cos angle between normal and light direction
    float NdotV; // cos angle between normal and view direction
    float NdotH; // cos angle between normal and half vector
    float LdotH; // cos angle between light direction and half vector
    float VdotH; // cos angle between view direction and half vector
    // r : alphaRoughness roughness mapped to a more linear change in the roughness (proposed by
    // [2]) g : perceptualRoughness value, as authored by the model creator (input to shader) -->
    // this is used to respect the model creator b : metallic value at the surface
    vec3 RoughnessesMetal;
    vec3 reflectance0;  // full reflectance color (normal incidence angle)
    vec3 reflectance90; // reflectance color at grazing angle
    vec3 diffuseColor;  // color contribution from diffuse lighting
    vec3 specularColor; // color contribution from specular lighting
};

vec3 getNormalCommon( GLTFCommon material, vec3 texCoord, vec3 N, vec3 T, vec3 B ) {
#ifdef TEXTURE_NORMAL
    mat3 tbn;
    tbn[0] = T;
    tbn[1] = B;
    tbn[2] = N;
#    ifdef TEXTURE_COORD_TRANSFORM_NORMAL
    vec3 ct          = material.normalTransform * vec3( texCoord.xy, 1 );
    vec3 normalLocal = ( texture( material.normal, ct.xy ).rgb * 2 - 1 ) *
                       vec3( material.normalTextureScale, material.normalTextureScale, 1 );
#    else
    vec3 normalLocal = ( vec3( texture( material.normal, texCoord.xy ) ) * 2 - 1 ) *
                       vec3( material.normalTextureScale, material.normalTextureScale, 1 );
#    endif
    return normalize( tbn * normalLocal );
#else
    return N;
#endif
}

vec3 modulateByAO( GLTFCommon material, vec3 color, vec3 texCoord ) {
#ifdef TEXTURE_OCCLUSION
// TODO : integrate IBL
// TODO : https://github.com/KhronosGroup/glTF-WebGL-PBR/issues/138
// https://github.com/KhronosGroup/glTF-Sample-Viewer
// https://github.com/KhronosGroup/glTF/issues/915
// https://github.com/KhronosGroup/glTF/issues/1427
// http://research.tri-ace.com/Data/cedec2011_RealtimePBR_Implementation_e.pptx
// only use AO on indirect lighting
#    ifdef TEXTURE_COORD_TRANSFORM_OCCLUSION
    vec3 ct = material.occlusionTransform * vec3( texCoord.xy, 1 );
#    else
    vec3 ct = vec3( texCoord.xy, 1 );
#    endif
    float ao = texture( material.occlusion, ct.xy ).r * material.occlusionStrength;
    return mix( color, color * ao, material.occlusionStrength );
#else
    return color;
#endif
}

vec3 getEmissiveColorCommon( GLTFCommon material, vec3 textCoord ) {
    vec3 e = material.emissiveFactor.rgb;
#ifdef TEXTURE_EMISSIVE
#    ifdef TEXTURE_COORD_TRANSFORM_EMISSIVE
    vec3 ct = material.emissiveTransform * vec3( textCoord.xy, 1 );
#    else
    vec3 ct = vec3( textCoord.xy, 1 );
#    endif
    e *= texture( material.emissive, ct.xy ).rgb;
#endif
    return e;
}

bool toDiscardCommon( GLTFCommon material, vec4 color ) {
    if ( material.alphaMode == 1 && color.a < material.alphaCutoff ) return true;
    if ( material.alphaMode == 2 && color.a < 1 ) return true;
    return false;
}

// compute F0 coef according to material spec
vec3 specularEnvR0( vec3 specularColor ) {
    return specularColor;
}

// compute F90 coef according to material spec (GLTF says 1, Schlick says what is in the comment)
vec3 specularEnvR90( vec3 specularColor ) {
    //    return vec3(1);

    float reflectance   = max( max( specularColor.r, specularColor.g ), specularColor.b );
    float reflectance90 = clamp( reflectance * 25.0, 0.0, 1.0 );
    return vec3( reflectance90 );
}

// Basic Lambertian diffuse
// Implementation from Lambert's Photometria https://archive.org/details/lambertsphotome00lambgoog
// See also [1], Equation 1
vec3 diffuse( PBRInfo pbrInputs ) {
    return pbrInputs.diffuseColor * InvPi;
}

// The following equation models the Fresnel reflectance term of the spec equation (aka F())
// Implementation of fresnel from [4], Equation 15
vec3 specularReflection( PBRInfo pbrInputs ) {
    return pbrInputs.reflectance0 + ( pbrInputs.reflectance90 - pbrInputs.reflectance0 ) *
                                        pow( clamp( 1.0 - pbrInputs.VdotH, 0.0, 1.0 ), 5.0 );
}

// This calculates the specular geometric attenuation (aka G()),
// where rougher material will reflect less light back to the viewer.
// This implementation is based on [1] Equation 4, and we adopt their modifications to
// alphaRoughness as input as originally proposed in [2].
float geometricOcclusion( PBRInfo pbrInputs ) {
    float NdotL = pbrInputs.NdotL;
    float NdotV = pbrInputs.NdotV;
#ifdef USE_CREATOR_ALPHA
    float r = pbrInputs.RoughnessesMetal.g;
#else
    float r           = pbrInputs.RoughnessesMetal.r;
#endif

    float attenuationL =
        2.0 * NdotL / ( NdotL + sqrt( r * r + ( 1.0 - r * r ) * ( NdotL * NdotL ) ) );
    float attenuationV =
        2.0 * NdotV / ( NdotV + sqrt( r * r + ( 1.0 - r * r ) * ( NdotV * NdotV ) ) );
    return attenuationL * attenuationV;
}

// The following equation(s) model the distribution of microfacet normals across the area being
// drawn (aka D()) Implementation from "Average Irregularity Representation of a Roughened Surface
// for Ray Reflection" by T. S. Trowbridge, and K. P. Reitz Follows the distribution function
// recommended in the SIGGRAPH 2013 course notes from EPIC Games [1], Equation 3.
float microfacetDistribution( PBRInfo pbrInputs ) {
#ifdef USE_CREATOR_ALPHA
    float roughnessSq = pbrInputs.RoughnessesMetal.g * pbrInputs.RoughnessesMetal.g;
#else
    float roughnessSq = pbrInputs.RoughnessesMetal.r * pbrInputs.RoughnessesMetal.r;
#endif
    float f = ( pbrInputs.NdotH * roughnessSq - pbrInputs.NdotH ) * pbrInputs.NdotH + 1.0;
    return roughnessSq / ( M_PI * f * f );
}

vec3 evaluateBsdf( PBRInfo pbrInputs ) {
    // Calculate the shading terms for the microfacet specular shading model
    vec3 F  = specularReflection( pbrInputs );
    float G = geometricOcclusion( pbrInputs );
    float D = microfacetDistribution( pbrInputs );

    // Calculation of analytical lighting contribution
    vec3 diffuseContrib = ( 1.0 - F ) * diffuse( pbrInputs );
    vec3 specContrib    = F * G * D / ( 4.0 * pbrInputs.NdotL * pbrInputs.NdotV );
    // Obtain final intensity as reflectance (BRDF) scaled by the energy of the light (cosine law)
    return pbrInputs.NdotL * ( diffuseContrib + specContrib );
}

/**** BSDF SEPARABLE INTERFACE */
// Note that diffuse and specular must not be multiplied by cos(wi) as this will be done when using
// de BSDF
vec3 diffuseBSDF( PBRInfo pbrInputs ) {
    vec3 F              = specularReflection( pbrInputs );
    vec3 diffuseContrib = ( 1.0 - F ) * diffuse( pbrInputs );
    return diffuseContrib;
}

vec3 specularBSDF( PBRInfo pbrInputs ) {
    // Calculate the shading terms for the microfacet specular shading model
    vec3 F  = specularReflection( pbrInputs );
    float G = geometricOcclusion( pbrInputs );
    float D = microfacetDistribution( pbrInputs );
    // Calculation of analytical lighting contribution
    vec3 specContrib = F * G * D / ( 4.0 * pbrInputs.NdotL * pbrInputs.NdotV );
    return specContrib;
}

int extractBaseBSDFParameters( GLTFCommon baseMaterial,
                               vec3 texC,
                               vec3 L,
                               vec3 V,
                               vec3 N,
                               out PBRInfo params ) {
    // Geometric code, common to MetallicRoughness and SpecularGlossiness
    float cosTo = dot( V, N );
    if ( baseMaterial.doubleSided == 1 )
    {
        if ( cosTo < 0 )
        {
            // back face fragment
            N *= -1;
            cosTo = clamp( -cosTo, 0.001, 1.0 );
        }
    }
    else
    {
        if ( cosTo < 0 ) { return 0; /*discard;*/ }
    }
    vec3 H = V + L;
    if ( length( H ) < 0.001 )
        H = N; /* TODO --> this could be buggy */
    else
        H = normalize( H );
    float cosTi = clamp( dot( L, N ), 0.001, 1.0 );
    params      = PBRInfo( cosTi,
                      cosTo,
                      clamp( dot( N, H ), 0.0, 1.0 ),
                      clamp( dot( L, H ), 0.0, 1.0 ),
                      clamp( dot( V, H ), 0.0, 1.0 ),
                      vec3( 0 ),
                      vec3( 0 ),
                      vec3( 0 ),
                      vec3( 0 ),
                      vec3( 0 ) );
    return 1;
}
#endif
