// This is the basic fragmentShader any PBR material can use.
// A specific material fragment shader implements the material interface (computeMaterialInternal)
// and include this shader

// This is for a preview of the shader composition, but in time we must use more specific Light
// Shader
#include "DefaultLight.glsl"

uniform sampler2D uShadowMap;

out vec4 fragColor;

#include "VertexAttribInterface.frag.glsl"
// -----------------
layout( location = 5 ) in vec3 in_viewVector;
layout( location = 6 ) in vec3 in_lightVector;

void main() {
    // discard non opaque fragment
    vec4 bc = getBaseColor( material, getPerVertexTexCoord() );
    if ( toDiscard( material, bc ) ) discard;

    // all vectors are in world space
    vec3 binormal    = getWorldSpaceBiTangent();
    vec3 normalWorld = getNormal(
        material, getPerVertexTexCoord(), getWorldSpaceNormal(), getWorldSpaceTangent(), binormal );
    vec3 binormalWorld = normalize( cross( normalWorld, getWorldSpaceTangent() ) );
    vec3 tangentWorld  = cross( binormalWorld, normalWorld );

    // A material is always evaluated in the fragment local Frame
    // compute matrix from World to local Frame
    mat3 world2local;
    world2local[0] = vec3( tangentWorld.x, binormalWorld.x, normalWorld.x );
    world2local[1] = vec3( tangentWorld.y, binormalWorld.y, normalWorld.y );
    world2local[2] = vec3( tangentWorld.z, binormalWorld.z, normalWorld.z );
    // transform all vectors in local frame so that N = (0, 0, 1);
    vec3 wi    = world2local * normalize( in_lightVector ); // incident direction
    vec3 wo    = world2local * normalize( in_viewVector );  // outgoing direction
    vec3 color = evaluateBSDF( material, getPerVertexTexCoord(), wi, wo );
    color      = color * lightContributionFrom( light, getWorldSpacePosition().xyz );
#ifdef USE_IBL
    color = modulateByAO( material.baseMaterial, color, getPerVertexTexCoord() );
#endif
    // color      = color + getEmissiveColor(material.baseMaterial, getPerVertexTexCoord());
    fragColor = vec4( color, 1.0 );
}
