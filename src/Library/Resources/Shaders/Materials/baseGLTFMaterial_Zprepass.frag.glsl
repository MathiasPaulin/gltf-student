layout( location = 0 ) out vec4 out_ambient;
layout( location = 1 ) out vec4 out_normal;
layout( location = 2 ) out vec4 out_diffuse;
layout( location = 3 ) out vec4 out_specular;

#include "VertexAttribInterface.frag.glsl"
layout( location = 5 ) in vec3 in_viewVector;
layout( location = 6 ) in vec3 in_lightVector;

//------------------- main ---------------------
void main() {
    // discard non opaque fragment
    vec4 bc = getDiffuseColor( material, in_texcoord );
    if ( toDiscard( material, bc ) ) discard;
    vec3 normalWorld   = getWorldSpaceNormal();    // normalized interpolated normal
    vec3 tangentWorld  = getWorldSpaceTangent();   // normalized tangent
    vec3 binormalWorld = getWorldSpaceBiTangent(); // normalized bitangent
    // Apply normal mapping
    normalWorld = getNormal( material,
                             getPerVertexTexCoord(),
                             normalWorld,
                             tangentWorld,
                             binormalWorld ); // normalized bump-mapped normal

    vec3 diffuse;
    vec3 specular;

    if ( getSeparateBSDFComponent( material,
                                   getPerVertexTexCoord(),
                                   normalize( in_lightVector ),
                                   normalize( in_viewVector ),
                                   normalWorld,
                                   diffuse,
                                   specular ) == 0 )
    { discard; }

    out_ambient  = vec4( diffuse * 0.01 + getEmissiveColor( material, in_texcoord ), 1.0 );
    out_normal   = vec4( normalWorld * 0.5 + 0.5, 1.0 );
    out_diffuse  = vec4( diffuse, 1.0 );
    out_specular = vec4( specular, 1.0 );
}
