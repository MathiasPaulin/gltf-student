#include <glTFLibrary.hpp>

#include <Engine/Data/MetallicRoughnessMaterial.hpp>
#include <Engine/Data/SpecularGlossinessMaterial.hpp>

namespace GLTF {
bool initializeGltf() {
    GLTFMaterial::registerMaterial();
    MetallicRoughness::registerMaterial();
    SpecularGlossiness::registerMaterial();
    return true;
}

bool finalizeGltf() {
    MetallicRoughness::unregisterMaterial();
    SpecularGlossiness::unregisterMaterial();
    return true;
}
} // namespace GLTF
