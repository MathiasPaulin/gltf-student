#pragma once
#include <glTFLibraryMacros.hpp>

namespace GLTF {
glTF_LIBRARY_API bool initializeGltf();
glTF_LIBRARY_API bool finalizeGltf();
} // namespace GLTF
