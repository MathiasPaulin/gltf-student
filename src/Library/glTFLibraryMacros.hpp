#pragma once
#include <Core/RaCore.hpp>
/// Defines the correct macro to export dll symbols.
#if defined RadiumGlTF_EXPORTS
#    define glTF_LIBRARY_API DLL_EXPORT
#else
#    define glTF_LIBRARY_API DLL_IMPORT
#endif
