#include <Gui/widget.hpp>
#include "ui_widget.h"

#include <QFileDialog>

#include <Engine/Scene/EntityManager.hpp>
#include <Engine/Scene/SystemDisplay.hpp>
#include <Gui/SelectionManager/SelectionManager.hpp>
#include <IO/Gltf/Writer/glTFFileWriter.hpp>

namespace GLTF {
static const QString separator {"_"};

GLTFExportWidget::GLTFExportWidget( const Ra::Gui::SelectionManager* selector, QWidget* parent ) :
    QWidget( parent ), ui( new Ui::GLTFExportWidget ), m_selector {selector} {
    ui->setupUi( this );
}

GLTFExportWidget::~GLTFExportWidget() {
    delete ui;
}

void GLTFExportWidget::chooseFile() {
    QString fileName =
        QFileDialog::getSaveFileName( this, tr( "Save as ..." ), "", tr( "GLTF 2.0 (*.gltf)" ) );
    auto selectedEntries = m_selector->selectedEntries();
    std::vector<Ra::Engine::Scene::Entity*> toExport;
    if ( selectedEntries.empty() )
    {
        // no entities selected, save the whole entities
        auto entityManager = Ra::Engine::RadiumEngine::getInstance()->getEntityManager();
        auto entities      = entityManager->getEntities();
        std::copy_if(
            entities.begin(), entities.end(), std::back_inserter( toExport ), []( auto e ) {
                return e != Ra::Engine::Scene::SystemEntity::getInstance();
            } );
    }
    else
    {
        std::for_each(
            selectedEntries.begin(), selectedEntries.end(), [&toExport]( const auto& e ) {
                if ( e.isEntityNode() ) { toExport.push_back( e.m_entity ); }
            } );
    }
    glTFFileWriter writer {fileName.toStdString(),
                           ui->texturePrefixInput->text().toStdString(),
                           ui->imageWriteOption->isChecked()};
    writer.write( toExport );
}
} // namespace GLTF
