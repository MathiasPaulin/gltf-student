#pragma once

#include <QWidget>
namespace Ra::Gui {
class SelectionManager;
}

namespace Ui {
class GLTFExportWidget;
}

namespace GLTF {
class GLTFExportWidget : public QWidget
{
    Q_OBJECT

  public:
    explicit GLTFExportWidget( const Ra::Gui::SelectionManager* selector,
                               QWidget* parent = nullptr );
    ~GLTFExportWidget() override;

  public slots:
    void chooseFile();

  private:
    Ui::GLTFExportWidget* ui;
    const Ra::Gui::SelectionManager* m_selector;
};

} // namespace GLTF
