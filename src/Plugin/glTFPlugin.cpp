#include <glTFLibrary.hpp>
#include <glTFPlugin.hpp>

#include <Gui/widget.hpp>

#include <Engine/RadiumEngine.hpp>
#include <Gui/SelectionManager/SelectionManager.hpp>
#include <IO/Gltf/Loader/glTFFileLoader.hpp>

//#include <globjects/DebugMessage.h>

namespace GLTF {
using namespace Ra::Core::Asset;

void glTFPlugin::registerPlugin( const Ra::Plugins::Context& context ) {
    m_selectionManager = context.m_selectionManager;
}

bool glTFPlugin::doAddWidget( QString& name ) {
    name = "GLTF 2.0 plugin";
    return true;
}

QWidget* glTFPlugin::getWidget() {
    return new GLTFExportWidget( m_selectionManager );
}

bool glTFPlugin::doAddMenu() {
    return false;
}

QMenu* glTFPlugin::getMenu() {
    return nullptr;
}

bool glTFPlugin::doAddAction( int& /*nb*/ ) {
    return false;
}

QAction* glTFPlugin::getAction( int /*id*/ ) {
    return nullptr;
}

bool glTFPlugin::doAddFileLoader() {
    return true;
}

void glTFPlugin::addFileLoaders( std::vector<std::shared_ptr<FileLoaderInterface>>* fl ) {
    fl->push_back( std::make_unique<glTFFileLoader>() );
}

bool glTFPlugin::doAddRenderer() {
    return false;
}

bool glTFPlugin::doAddROpenGLInitializer() {
    return true;
}

void glTFPlugin::openGlInitialize( const Ra::Plugins::Context& context ) {
    GLTF::initializeGltf();
    //    globjects::DebugMessage::enable(); // enable automatic messages if KHR_debug is available
}

glTFPlugin::~glTFPlugin() {
    GLTF::finalizeGltf();
}
} // namespace GLTF
