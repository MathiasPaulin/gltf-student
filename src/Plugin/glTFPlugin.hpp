#pragma once
#include <glTFPluginMacros.hpp>

#include <Core/CoreMacros.hpp>
#include <PluginBase/RadiumPluginInterface.hpp>

namespace GLTF {

/*!
 * \brief This plugin defines a file loader for glTF2.0 format.
 * Plugin that bring support for GLTF 2.0 in Radium.
 * See file Readme.md for information about whet is supported from GLTF.
 * This plugin also allow to export Radium scenes in GLTF.
 *
 */
class glTF_PLUGIN_API glTFPlugin : public QObject, Ra::Plugins::RadiumPluginInterface
{
    Q_OBJECT
    Q_RADIUM_PLUGIN_METADATA
    Q_INTERFACES( Ra::Plugins::RadiumPluginInterface )

  public:
    glTFPlugin() = default;

    ~glTFPlugin() override; // = default;

    void registerPlugin( const Ra::Plugins::Context& context ) override;

    bool doAddWidget( QString& name ) override;
    QWidget* getWidget() override;
    bool doAddMenu() override;
    QMenu* getMenu() override;
    bool doAddAction( int& nb ) override;
    QAction* getAction( int id ) override;
    bool doAddRenderer() override;

    bool doAddFileLoader() override;
    void addFileLoaders(
        std::vector<std::shared_ptr<Ra::Core::Asset::FileLoaderInterface>>* fl ) override;

    bool doAddROpenGLInitializer() override;

    void openGlInitialize( const Ra::Plugins::Context& context ) override;

  private:
    Ra::Gui::SelectionManager* m_selectionManager {nullptr};
};

} // namespace GLTF
