#pragma once
#include <Core/RaCore.hpp>
/// Defines the correct macro to export dll symbols.
#if defined RadiumGlTFPlugin_EXPORTS
#    define glTF_PLUGIN_API DLL_EXPORT
#else
#    define glTF_PLUGIN_API DLL_IMPORT
#endif
