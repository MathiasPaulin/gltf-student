#include "OpenGlContext.hpp"

#include <globjects/globjects.h>

#include <GLFW/glfw3.h>

static void error( int errnum, const char* errmsg ) {
    globjects::critical() << errnum << ": " << errmsg << std::endl;
}

OpenGLContext::OpenGLContext() {
    // initialize openGL
    if ( glfwInit() )
    {
        glfwSetErrorCallback( error );
        glfwDefaultWindowHints();
        glfwWindowHint( GLFW_VISIBLE, false );
        glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
        glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 1 );
        glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );
        glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
        m_offscreenContext =
            glfwCreateWindow( 256, 256, "Radium CommandLine Context", nullptr, nullptr );
    }
    if ( m_offscreenContext == nullptr )
    {
        globjects::critical() << "Context creation failed. Terminate execution.";
        glfwTerminate();
    }
    else
    {
        glfwMakeContextCurrent( m_offscreenContext );
        // Initialize globjects (internally initializes glbinding, and registers the current
        // context)
        globjects::init( []( const char* name ) { return glfwGetProcAddress( name ); } );
    }
}
OpenGLContext::~OpenGLContext() {
    glfwTerminate();
}
void OpenGLContext::makeCurrent() {
    if ( m_offscreenContext ) { glfwMakeContextCurrent( m_offscreenContext ); }
}
bool OpenGLContext::isValid() {
    return m_offscreenContext != nullptr;
}
