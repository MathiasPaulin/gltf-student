#pragma once

class GLFWwindow;

class OpenGLContext
{
  public:
    OpenGLContext();
    ~OpenGLContext();
    void makeCurrent();
    bool isValid();

  private:
    GLFWwindow* m_offscreenContext {nullptr};
    int x {A};
};
